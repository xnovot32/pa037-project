#ifndef PARSER_HPP
#define PARSER_HPP

#include "Lexer.hpp"
#include <assert.h>
#include <exception>
#include <map>
#include <sstream>
#include <string>
#include <vector>

enum SymbolType {
    ST_VARIABLE,
    ST_PARAMETER,
    ST_FUNCTION,
    ST_UNDEFINED,
    ST_METADATA
};

enum DataType {
    DT_INTEGER,
    DT_DOUBLE,
    DT_ARBITRARY
};

struct SymbolData {
    SymbolType symbol_type;
    std::vector<DataType> data_type;
    size_t offset;
};

class SymbolTable {
private:
    std::vector<std::pair<std::string, std::map<std::string, SymbolData> > > blocks;

public:
    void push(std::string block_name)
    {
        blocks.emplace_back(std::pair<std::string, std::map<std::string, SymbolData> >{ block_name, std::map<std::string, SymbolData>{} });
    }
    std::string pop()
    {
        assert(!blocks.empty());
        const auto& block = blocks.back();
        blocks.pop_back();
        return block.first;
    }
    std::string scope_name()
    {
        std::ostringstream name;
        for (const auto& block : blocks) {
            if (!block.first.empty())
                name << block.first << "::";
        }
        return name.str();
    }
    std::pair<bool, SymbolData&> find(std::string name)
    {
        assert(!blocks.empty());
        for (std::vector<std::pair<std::string, std::map<std::string, SymbolData> > >::reverse_iterator iter = blocks.rbegin();
             iter != blocks.rend();) {
            auto& map = iter++->second;
            auto search = map.find(name);
            if (search != map.end()) {
                if (iter == blocks.rend())
                    return std::pair<bool, SymbolData&>{ true, search->second };
                else
                    return std::pair<bool, SymbolData&>{ false, search->second };
            }
        }
        auto insert_result = blocks.back().second.insert(std::pair<std::string, SymbolData>(
            name, { ST_UNDEFINED, { DT_ARBITRARY }, 0 }));
        assert(insert_result.second);
        return { blocks.size() == 1, insert_result.first->second };
    }
    bool insert(std::string name, SymbolData data)
    {
        assert(!blocks.empty());
        return blocks.back().second.insert(std::pair<std::string, SymbolData>(name, data)).second;
    }
};

class Parser {
protected:
    Lexer& lexer;
    std::ostream& out;
    std::ostream& err;
    void warn(std::string msg);
    void error(std::string msg);

    // Used to dynamically switch the user-specified Parser::out output stream
    // for string streams to capture the parser output.
    std::vector<std::ostringstream> out_streams{};
    void start_buffering()
    {
        out_streams.emplace_back(std::ostringstream{});
    }
    std::ostringstream stop_buffering()
    {
        std::ostringstream out_stream = std::move(out_streams.back());
        assert(!out_streams.empty());
        out_streams.pop_back();
        return out_stream;
    }
    std::ostream& get_current_out()
    {
        if (out_streams.empty())
            return out;
        else
            return out_streams.back();
    }

    std::invalid_argument unexpected_token(const Token& actual, const std::string& msg);
    std::invalid_argument expected_tokens(const Token& actual, const std::vector<Term>& expected);
    std::invalid_argument undefined_variable(const Token& name);
    std::invalid_argument multiply_defined_variable(const Token& name);
    std::invalid_argument undefined_function(const Token& name);
    std::invalid_argument multiply_defined_function(const Token& name);

    std::invalid_argument type_error(const Token& where, std::string msg);
    std::invalid_argument cannot_coerce(const Token& where, std::string from, std::string to);
    std::invalid_argument extra_parameter(const Token& where);
    std::invalid_argument missing_parameter(const Token& where);

    SymbolTable symbols;

public:
    virtual ~Parser() = default;
    Parser(Lexer& lexer, std::ostream& out, std::ostream& err)
        : lexer(lexer)
        , out(out)
        , err(err)
    {
    }
    virtual void run() = 0;
};

#endif // PARSER_HPP
