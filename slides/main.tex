\documentclass[aspectratio=169,t]{beamer}
\usetheme[faculty=fi,nofonts]{fibeamer}
\setbeamertemplate{caption}[numbered]

% Localization and fonts
\usepackage[default,scale=0.96]{lato}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{minted}
\usepackage{fancyvrb}

% Bibliography
\usepackage[
  backend=biber
 ,style=authoryear
 ,sortlocale=cs_CZ
 ,autolang=other
 ,bibencoding=UTF8
]{biblatex}
\usepackage{filecontents}
\begin{filecontents}{main.bib}
@book{aho86,
  author = {Aho, Alfred V. and Sethi, Ravi and Ullman, Jeffrey D.},
  title = {Compilers: Principles, Techniques, and Tools},
  year = {1986},
  isbn = {0-201-10088-6},
  publisher = {Addison-Wesley Longman Publishing Co., Inc.},
  address = {Boston, MA, USA}}
@online{novotny17,
  author = {Vít Novotný},
  title = {pa037-project: A programming language compiler for
    the PA037 Compiler Project course.},
  year = {2017},
  url = {https://gitlab.fi.muni.cz/xnovot32/pa037-project},
  urldate = {2017-06-25}}
@online{parr,
  author = {Terence Parr},
  title = {ANTRL3: ANTLR Parser Generator},
  url = {http://www.antlr3.org/},
  urldate = {2017-06-25}}
@inbook{lesk75,
  author = {Lesk, M.E.; Schmidt, E.},
  title = {Lex – A Lexical Analyzer Generator},
  booktitle = {Unix Time-Sharing System: Unix Programmer's Manual},
  date = {1975-07-21},
  edition = {7},
  volume = {2B},
  url = {http://epaperpress.com/lexandyacc/download/lex.pdf},
  urldate = {2017-06-25}}
@report{mcilroy87,
  author = {McIlroy, M. D.},
  title = {A Research Unix reader: annotated excerpts from the Programmer's Manual, 1971--1986},
  year = {1987},
  publisher = {Bell Labs},
  url = {http://www.cs.dartmouth.edu/~doug/reader.pdf},
  urldate = {2017-06-25}}
@inbook{lattner12,
  author = {Chris Lattner},
  editor = {Amy Brown and Greg Wilson},
  title = {LLVM},
  booktitle = {The Architecture of Open Source Applications},
  year = {2012},
  volume = {1},
  url = {http://www.aosabook.org/en/llvm.html},
  urldate = {2017-06-25}}
@book{abelson96,
  author = {Abelson, Harold and Sussman, Gerald J.},
  title = {Structure and Interpretation of Computer Programs},
  year = {1996},
  isbn = {0262011530},
  edition = {2},
  publisher = {MIT Press},
  address = {Cambridge, MA, USA}}

@online{commandguide,
  title = {LLVM Command Guide},
  url = {http://llvm.org/docs/CommandGuide/},
  urldate = {2017-06-25}}
@online{langmanual,
  title = {LLVM Language Reference Manual},
  url = {http://llvm.org/docs/LangRef.html},
  urldate = {2017-06-25}}
@online{kaleidoscope,
  title = {Kaleidoscope: Tutorial Introduction and the Lexer},
  url = {http://llvm.org/docs/tutorial/LangImpl01.html},
  urldate = {2017-06-25}}
@online{anderson09,
  title = {LLVM Tutorial 1: A First Function},
  author = {Owen Anderson},
  date = {2009-10-24},
  url = {http://releases.llvm.org/2.6/docs/tutorial/JITTutorial1.html},
  urldate = {2017-06-25}}
@online{bendersky17,
  title = {llvm-clang-samples: Examples of using the LLVM and Clang compilation
    libraries and tools},
  author = {Eli Bendersky},
  url = {https://github.com/eliben/llvm-clang-samples},
  urldate = {2017-06-25}}
@online{smith15,
  title = {How to get started with the LLVM C API},
  author = {Paul Smith},
  date = {2015-01-20},
  url = {https://pauladamsmith.com/blog/2015/01/how-to-get-started-with-llvm-c-api.html},
  urldate = {2017-06-25}}
@online{duran14,
  title = {Learn the LLVM C++ API by example},
  author = {Eloy Durán},
  date = {2014-08-08},
  url = {https://gist.github.com/alloy/d86b007b1b14607a112f},
  urldate = {2017-06-25}}
\end{filecontents}
\addbibresource{main.bib}

% Metadata
\title{A Compiler for \textcolor{white}{\RULA}
  (the \textcolor{white}{\RU}dimentary \textcolor{white}{\LA}nguage)}
\subtitle{FI:PA037 Compiler Project, Spring 2017}
\author{Vít Novotný}

% Own commands
\def\RU{\ensuremath{{\cal RU\kern-1pt}}}
\def\LA{\ensuremath{{\cal L\kern-1ptA}}}
\def\RULA{\RU\kern-1pt\LA}

\begin{document}
\frame[c]{\maketitle}
\AtBeginSection[]{\frame[c]{\sectionpage}}

\begin{frame}{\contentsname}
  \tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}
\frametitle{\secname}
\RULA\ is a programming language that
\begin{itemize}
  \item was inspired by the example language from \textcite{aho86},
  \item is recognized by an SLL(1) grammar (boring from the formal language viewpoint),
  \item is strongly, statically typed and statically scoped with pass-by-value calls,
  \item supports only numeric data types (32bit signed integers and 64bit floats),
  \item has an XML parser / compiler to LLVM in $3.2$\,kLOC of C++14 code \autocite{novotny17}.
\end{itemize}

Unusual features of \RULA's syntax include
\begin{itemize}
  \item functions with mandatory return types (e.\,g. no support for
    procedures),
  \item second-class booleans that cannot be stored inside variables, cannot be
    mixed with arithmetic expressions, and have their own parentheses for
    nesting.
  \item top-level \texttt{return} statements automatically \texttt{printf} the value,
  \item the equals sign (\texttt{=}) is used both for assignments and for
    equality testing.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{A demonstration of \RULA\ and \textcite{novotny17} -- \texttt{LLVMParser.cpp}}
\vspace{-1.5em}
\begin{verbatim}
# apt install llvm cmake
\end{verbatim}
\vspace{-1.7em}
\begin{minted}{bash}
$ git clone https://gitlab.fi.muni.cz/xnovot32/pa037-project.git
$ DIR=pa037-project/build; mkdir $DIR; cd $DIR; cmake ..; make
$ cat >factorial.rl <<EOF
> function fac(i : integer) : integer
>   if [ i > 1 ] then
>     return i * (call fac(i + (-1)));
>   else return 1; fi; end;
> return call fac(5);
> EOF
> ./main <factorial.rl >factorial.ll
> lli factorial.ll
120
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{A demonstration of \RULA\ and \textcite{novotny17} -- \texttt{XMLParser.cpp}}
\vspace{-1.1em}
\begin{verbatim}
> ./main --xml <factorial.rl >factorial.xml
> cat factorial.xml
\end{verbatim}
\vspace{-1em}
\begin{minted}{xml}
<block><functions><function><name>fac</name>
  <parameters><parameter><name>i</name><type>integer</type>
  </parameter></parameters>
  <type>integer</type>
  <block><statements><statement>
    <if><relation_binary_operator_greater_than>
      <left_operand><identifier>i</identifier></left_operand>
      <right_operand><integer_constant>1</integer_constant>
      </right_operand></relation_binary_operator_greater_than>
      <statements><statement>
        <return><arithmetic_binary_operator_multiplication>
          ...
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{A demonstration of \RULA\ and \textcite{novotny17} -- \texttt{tests/*.cpp}}
\vspace{-1.5em}
\begin{verbatim}
> ./run-tests
Running main() from gtest_main.cc
[==========] Running 52 tests from 3 test cases.
[----------] Global test environment set-up.
[----------] 7 tests from Phase01_Lexer
[ RUN      ] Phase01_Lexer.characterTokens
...
[       OK ] Phase03_LLVMParser.relationExpressions (216 ms)
[----------] 27 tests from Phase03_LLVMParser (1170 ms total)

[----------] Global test environment tear-down
[==========] 52 tests from 3 test cases ran. (1198 ms total)
[  PASSED  ] 52 tests.
\end{verbatim}
\end{frame}

\section{Developer diary}
\subsection{Drafting a grammar}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname}
I used ANTLR3~\autocite{parr} to draft \RULA's grammar. The drafted grammar
was disambiguated, left recursion was replaced with right recursion and
unpredictable branching was removed via left-factoring, see
\autocite[sec.\,4.3]{aho86}. After computing FIRST$_1$ $\oplus$ FOLLOW$_1$ for
all rules, the grammar was shown to be SLL(1).
\begin{minted}{bash}
$ cd ..; git checkout 5dfdca0; cat grammar-draft.g
\end{minted}
\vspace{-0.7em}
\begin{minted}{antlr}
ID      : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;
INTEGER : ('0'..'9')*;
REAL    : INTEGER('.'INTEGER)?;
init    : block '\n';
block   : variable_declaration_list callable_declaration_list
          statement_list;
variable_declaration_list
        : variable_declaration ';' variable_declaration_list | ;
\end{minted}
\vspace{-1em}
\verb|...|
\end{frame}

\subsection{Building a lexer}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname}
Rather than use a prebuilt lexer such as Yacc~\autocite{lesk75}, I decided to
hand-build my own lexer, see \autocite[chap.\,3]{aho86}. The lexer is defined
in files \texttt{Lexer.hpp} and \texttt{Lexer.cpp}.
\begin{minted}{bash}
$ git checkout f4a710b; cd build; cmake ..; make
$ ./main <<< 'while i < j; do i = i + 1; done'
(14, while) at line 1, character number 1
$ ./main <<< [
(2, Left bracket ('[')) at line 1, character number 1
$ ./main <<< {
Lexer warning:  Unexpected character sequence encountered:  {
(17, {) at line 1, character number 1
\end{minted}
\end{frame}

\subsection{Building a parser}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname\ (i)}
Rather than use a prebuilt parser such as Yacc~\autocite{mcilroy87}, I decided to
hand-build my own parser as well, see \autocite[sec.\,4.4]{aho86}. The parser
uses recursive descent to parse \RULA's grammar and is defined in files
\texttt{XMLParser.hpp} and \texttt{XMLParser.cpp}. The parser produces an XML
serialization of the input source code on the output. An initial version
produces a parse tree:
\begin{minted}{bash}
$ git checkout e92ea42; cmake ..; make
$ ./main <<< 'variable a : integer;'
<init><block><variable_declaration_list><variable_declaration>
<variable_name>a</variable_name><type>integer</type>
</variable_declaration><variable_declaration_list>
</variable_declaration_list></variable_declaration_list>
<callable_declaration_list></callable_declaration_list>
<statement_list></statement_list></block></init>
\end{minted}
\end{frame}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname\ (ii)}
A later version produces a syntax tree, see \autocite[sec\,5.2]{aho86}:
\begin{minted}{bash}
$ git checkout 7f3128b; cmake ..; make
$ ./main <<< 'variable a : integer;'
<block><variables><variable><name>a</name><type>integer</type>
</variable></variables></block>
\end{minted}
Apart from building the output, the parser checks for undefined functions and
variables, and for extra and missing parameters to function calls. A later version
implements type checking and type coercions of R-values, see
\autocite[sec\,6.4]{aho86}:
\begin{minted}{bash}
$ git checkout 82d1289; cmake ..; make
$ ./main <<< 'function f() : integer return 1.0 end;'
Parser error: Type error from (Identifier, "return") at line 1,
              character number 24: Cannot coerce double to integer.
\end{minted}
\end{frame}

\subsection{Building a code generator}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname}
To create a code generator I reused the XML Parser code. (It would be cleaner
to design an interface between the parser and the code generator to avoid
duplication.) I decided to use LLVM~\autocite{lattner12} as the target
intermediary representation.

LLVM exposes a C++ interface that can be used to dynamically build the
intermediary representation. However, the interface is a part of the LLVM
codebase and therefore unstable across major releases. More stable C
bindings exist; however, the most stable option appears to be the manual
generation of the intermediary code, which is what the code generator defined
in files \texttt{LLVMParser.hpp} and \texttt{LLVMParser.cpp} does:
\begin{minted}{bash}
$ git checkout b7826f0; cmake ..; make
$ ./main <<< ''
define i32 @main() { ret i32 0 }
\end{minted}

A major new component necessary to produce intermediary code was the
control-flow translation of boolean expressions, see
\autocite[sec\,8.3]{aho86}.
\end{frame}

\section{Code examples}
\subsection{Computing the factorial}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname}
\begin{Verbatim}[numbers=left,numbersep=10pt]
variable i : integer;

function fac(i : integer) : integer
  if i > 1 then
    return i *  (call fac(i + (-1)));
  else
    return 1;
  fi;
end;

i = call fac(5);
return i;
\end{Verbatim}
\end{frame}

\subsection{Primality testing}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname}
\begin{Verbatim}[numbers=left,numbersep=10pt]
function smallest_factor(i : integer) : integer
  variable j : integer;
  j = 2;
  while i > j or i = j do
    if i mod j = 0 then
      return j;
    fi;
    j = j + 1;
  done;
end;

return call smallest_factor(739 * 739);
\end{Verbatim}
\end{frame}

\subsection{Counting change}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname~\autocite[sec.\,1.2.2]{abelson96}}
\vspace{-0.5em}
\scriptsize
\begin{Verbatim}[numbers=left,numbersep=5pt]
function first_denominator(kinds_of_coins : integer) : integer
  if kinds_of_coins = 1 then
    return 1;
  else if kinds_of_coins = 2 then
    return 5;
  else if kinds_of_coins = 3 then
    return 10;
  else if kinds_of_coins = 4 then
    return 25;
  else if kinds_of_coins = 5 then
    return 50;
  fi; fi; fi; fi; fi; end;

\end{Verbatim}
\vspace{-1.5em}
\begin{Verbatim}[firstnumber=25,numbers=left,numbersep=5pt]
function count_change(amount : integer) : integer
  return call cc(amount, 5); end;

return call count_change(100);
\end{Verbatim}
\vspace{-5.4cm}
\hspace{6.4cm}\begin{minipage}\textwidth
\begin{Verbatim}[firstnumber=14,numbers=left,numbersep=5pt]
function cc(amount         : integer,
            kinds_of_coins : integer) : integer
  if amount = 0 then
    return 1;
  else if amount < 0 or kinds_of_coins = 0 then
    return 0;
  else
    return (call cc(amount, kinds_of_coins + (-1)))
         + (call cc(amount + -(call first_denominator(
            kinds_of_coins)), kinds_of_coins));
  fi; fi; end;
\end{Verbatim}
\end{minipage}
\end{frame}

\subsection{Approximating $\sqrt[3]{10}$ via a quasi-newton method}
\begin{frame}[fragile]
\frametitle{\secname}
\framesubtitle{\subsecname}
\vspace{-0.5em}
\scriptsize
\begin{Verbatim}[numbers=left,numbersep=5pt]
variable iter      : integer;
variable max_iter  : integer;
variable delta     : double;
variable min_delta : double;
variable xk        : double;
variable xk_1      : double;
variable xk_2      : double;

function abs(x : double) : double
  if x < 0 then
    return -x;
  fi;
  return x;
end;

function f(x : double) : double
  return x*x*x + -10;

end;
\end{Verbatim}
\vspace{-6.47cm}
\hspace{6.4cm}\begin{minipage}\textwidth
\begin{Verbatim}[firstnumber=20,numbers=left,numbersep=5pt]
iter = 1;
max_iter = 999;
min_delta = 0.0000001;
delta = min_delta + 1;
xk = 2;
xk_1 = 3;

while iter < max_iter and delta > min_delta do
  xk_2 = xk_1 + -((xk_1 + -xk) / ((call f(xk_1))
                                 + -(call f(xk))))
              * (call f(xk_1));
  xk = xk_1;
  xk_1 = xk_2;
  iter = iter + 1;
  delta = call abs(xk_1 + -xk);
end;

return xk;
\end{Verbatim}
\end{minipage}
\end{frame}

\setbeamertemplate{frametitle continuation}{%
  (\MakeLowercase{\insertcontinuationcountroman})}
\section{Bibliography}
\begin{frame}[allowframebreaks]
\frametitle{\secname}
\nocite{*}
\printbibliography
\end{frame}

\end{document}
