grammar spl;

// Tokens
ID	:	('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;
INTEGER	:	('0'..'9')+;
REAL	:	('0'..'9')*.('0'..'9')+;

// Grammar
init	:	block '\n';                                                         // FIRST+FOLLOW = ID('variable'), ID('function'), ID('if'), ID('while'), ID('return'), ID(???), '\n'
block	:	variable_declaration_list callable_declaration_list statement_list; // FIRST+FOLLOW = ID('variable'), ID('function'), ID('if'), ID('while'), ID('return'), ID(???), '\n', ID('end')

/// Variable declarations
variable_declaration_list
	:	variable_declaration ';' variable_declaration_list // FIRST+FOLLOW = ID('variable')
	|	                                                   // FIRST+FOLLOW = ID('function'), ID(???), ID('if'), ID('while'), ID('return'), '\n', ID('end')
	;

variable_declaration
	:	'variable' ID ':' type // FIRST+FOLLOW = ID('variable')
	;

type
	:	'integer' // FIRST+FOLLOW = ID('integer')
	|	'double'  // FIRST+FOLLOW = ID('double')
//	|	'array' '[' NUM ']' 'of' type
//	|	type '*'
//	|	'auto'
	;

/// Procedure declaration list
callable_declaration_list
	:	callable_declaration ';' callable_declaration_list_tail // FIRST+FOLLOW = ID('function')
	|                                                         // FIRST+FOLLOW = ID(???), ID('if'), ID('while'), ID('return'), '\n', ID('end')
	;

callable_declaration
	:	'function' ID '(' parameter_list ')' ':' type block 'end' // FIRST+FOLLOW = ID('function')
	;

parameter_list
	:	parameter parameter_list_tail // FIRST+FOLLOW = ID(???)
	|                               // FIRST+FOLLOW = ')'
	;
parameter_list_tail
	:	',' parameter parameter_list_tail // FIRST+FOLLOW = ','
  |                                   // FIRST+FOLLOW = ')'
	;

parameter
	:	ID ':' type                       // FIRST+FOLLOW = ID(???)
	;

/// Statement list
statement_list
	:	statement ';' statement_list // FIRST+FOLLOW = ID(???), ID('if'), ID('while'), ID('return')
	|                              // FIRST+FOLLOW = '\n', ID('end'), ID('fi'), ID('else'), ID('done')
	;

statement
	:	ID '=' arithmetic_expression                                 // FIRST+FOLLOW = ID(???)
	|	'if' bool_expression 'then' statement_list statement_if_tail // FIRST+FOLLOW = ID('if')
	|	'while' bool_expression 'do' statement_list 'done'           // FIRST+FOLLOW = ID('while')
	|	'return' arithmetic_expression                               // FIRST+FOLLOW = ID('return')
	;

statement_if_tail
	:	'fi'                       // FIRST+FOLLOW = ID('fi')
	|	'else' statement_list 'fi' // FIRST+FOLLOW = ID('else')
	;

/// Arithmetic expressions
arithmetic_expression
	:	'call' ID '(' arithmetic_expression_list ')' // FIRST+FOLLOW = ID('call')
	|	arithmetic_factor arithmetic_expression_tail // FIRST+FOLLOW = ID(???), INTEGER, REAL, '(', '-'
	;
arithmetic_expression_tail
	:	'+' arithmetic_expression // FIRST+FOLLOW = '+'
	|                                 // FIRST+FOLLOW = ',', ')', '<', '=', '>', ';', ID('and'), ID('or'), ']', ID('then'), ID('do')
	;

arithmetic_expression_list
	:	arithmetic_expression arithmetic_expression_list_tail // FIRST+FOLLOW = ID('call'), ID(???), INTEGER, REAL, '(', '-'
	|                                                       // FIRST+FOLLOW = ')'
	;
arithmetic_expression_list_tail
	:	',' arithmetic_expression arithmetic_expression_list_tail // FIRST+FOLLOW = ','
	|                                                           // FIRST+FOLLOW = ')'
	;

arithmetic_factor
	:	arithmetic_operand arithmetic_factor_tail // FIRST+FOLLOW = ID(???), INTEGER, REAL, '(', '-'
	;
arithmetic_factor_tail
	:	('*'|'/'|'mod') arithmetic_factor // FIRST+FOLLOW = '*', '/', ID('mod')
	|                                   // FIRST+FOLLOW = '+', '-', ',', ')', '<', '=', '>', ';', ID('and'), ID('or'), ']', ID('then'), ID('do')
	;

arithmetic_operand
	:	ID                            // FIRST+FOLLOW = ID(???)
	|	INTEGER                       // FIRST+FOLLOW = INTEGER
	|	REAL                          // FIRST+FOLLOW = REAL
	|	'(' arithmetic_expression ')' // FIRST+FOLLOW = '('
//	|	arithmetic_expression '[' arithmetic_expression ']'
//	|	'*' arithmetic_expression
	|	'-' arithmetic_operand        // FIRST+FOLLOW = '-'
	;

/// Boolean expressions
bool_expression
	:	bool_factor bool_expression_tail // FIRST+FOLLOW = ID('true'), ID('false'), ID('call'), ID(???), INTEGER, REAL, '(', '-', '[', ID('not')
	;
bool_expression_tail
	:	'or' bool_expression // FIRST+FOLLOW = ID('or')
	|                      // FIRST+FOLLOW = ID('then'), ID('do'), ']'
	;

bool_factor
	:	bool_operand bool_factor_tail // FIRST+FOLLOW = ID('true'), ID('false'), ID('call'), ID(???), INTEGER, REAL, '(', '-', '[', ID('not')
	;
bool_factor_tail
	:	'and' bool_factor // FIRST+FOLLOW = ID('and')
	|                   // FIRST+FOLLOW = ID('or'), ID('then'), ID('do'), ']'
	;

bool_operand
	:	('true'|'false')        // FIRST+FOLLOW = ID('true'), ID('false')
	|	relation_expression     // FIRST+FOLLOW = ID('call'), ID(???), INTEGER, REAL, '(', '-'
	|	'[' bool_expression ']' // FIRST+FOLLOW = '['
	|	'not' bool_operand      // FIRST+FOLLOW = ID('not')
	;

/// Relation expressions
relation_expression
	:	arithmetic_expression relation_expression_tail // FIRST+FOLLOW = ID('call'), ID(???), INTEGER, REAL, '(', '-'
	;
relation_expression_tail
	:	('<'|'='|'>') arithmetic_expression // FIRST+FOLLOW = '<', '=', '>'
	;
