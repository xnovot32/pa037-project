#include "LLVMParser.hpp"
#include "Lexer.hpp"
#include <assert.h>
#include <iostream>

DataType LLVMParser::arithmetic_binary_operator_coerce(
    DataType left_operand_type, DataType right_operand_type,
    size_t& left_operand_storage_offset,
    size_t& right_operand_storage_offset)
{
    assert(left_operand_type != DT_ARBITRARY);
    assert(right_operand_type != DT_ARBITRARY);
    if (left_operand_type == DT_INTEGER && right_operand_type == DT_INTEGER) {
        return DT_INTEGER;
    } else {
        if (left_operand_type == DT_INTEGER) {
            size_t offset = llvm_coerce_i32_to_double(left_operand_storage_offset);
            left_operand_storage_offset = offset;
        }
        if (right_operand_type == DT_INTEGER) {
            size_t offset = llvm_coerce_i32_to_double(right_operand_storage_offset);
            right_operand_storage_offset = offset;
        }
        return DT_DOUBLE;
    }
}
DataType LLVMParser::return_data_type_coerce(
    const Token& where, DataType expected_type, DataType actual_type, size_t& storage_offset)
{
    assert(actual_type != DT_ARBITRARY);
    if (expected_type == DT_ARBITRARY) {
        return actual_type;
    }
    if (expected_type == actual_type || (expected_type == DT_DOUBLE && actual_type == DT_INTEGER)) {
        if (expected_type == DT_DOUBLE && actual_type == DT_INTEGER) {
            size_t offset = llvm_coerce_i32_to_double(storage_offset);
            storage_offset = offset;
        }
        return expected_type;
    } else if (expected_type == DT_INTEGER && actual_type == DT_DOUBLE) {
        throw cannot_coerce(where, "double", "integer");
    }
    assert(false); // Should never reach here.
}

size_t LLVMParser::llvm_store(size_t storage_offset, DataType type)
{
    assert(type != DT_ARBITRARY);
    size_t new_storage_offset = get_next_offset();
    if (type == DT_INTEGER)
        get_current_out() << "%identifier." << new_storage_offset << " = alloca i32, align 4" << std::endl
                          << "store i32 %identifier." << storage_offset
                          << ", i32* %identifier." << new_storage_offset << ", align 4" << std::endl;
    else
        get_current_out() << "%identifier." << new_storage_offset << " = alloca double, align 8" << std::endl
                          << "store double %identifier." << storage_offset
                          << ", double* %identifier." << new_storage_offset << ", align 8" << std::endl;
    return new_storage_offset;
}
size_t LLVMParser::llvm_load(size_t storage_offset, DataType type)
{
    assert(type != DT_ARBITRARY);
    size_t new_storage_offset = get_next_offset();
    if (type == DT_INTEGER)
        get_current_out() << "%identifier." << new_storage_offset
                          << " = load i32, i32* %identifier."
                          << storage_offset << ", align 4" << std::endl;
    else
        get_current_out() << "%identifier." << new_storage_offset
                          << " = load double, double* %identifier."
                          << storage_offset << ", align 8" << std::endl;
    return new_storage_offset;
}
size_t LLVMParser::llvm_coerce_i32_to_double(size_t storage_offset)
{
    size_t loaded_storage_offset = llvm_load(storage_offset, DT_INTEGER);
    size_t offset = get_next_offset();
    get_current_out() << "%identifier." << offset << " = sitofp i32 %identifier."
                      << loaded_storage_offset << " to double" << std::endl;
    return llvm_store(offset, DT_DOUBLE);
}

void LLVMParser::llvm_write_label(size_t label)
{
    get_current_out() << "label." << label << ":" << std::endl;
}
void LLVMParser::llvm_goto(size_t label)
{
    get_current_out() << "br label %label." << label << std::endl;
}
void LLVMParser::llvm_conditional_goto(
    size_t loaded_storage_offset, size_t label_false, size_t label_true)
{
    get_current_out() << "br i1 %identifier." << loaded_storage_offset
                      << ", label %label." << label_true
                      << ", label %label." << label_false << std::endl;
}

void LLVMParser::run()
{
    grammar_init();
}

void LLVMParser::grammar_init()
{
    symbols.push("");
    assert(symbols.insert("metadata::return_data_type", { ST_METADATA, { DT_ARBITRARY }, 0 }));
    get_current_out() << "declare i32 @printf(i8*, ...)" << std::endl
                      << "@.print_integer = private unnamed_addr constant [4 x i8] c\"%d\\0A\\00\", align 1" << std::endl
                      << "@.print_double = private unnamed_addr constant [5 x i8] c\"%lf\\0A\\00\", align 1" << std::endl;
    grammar_block();
    symbols.pop();
    auto token = lexer.getNext();
    auto eof = std::char_traits<char>::eof();
    if (token.term.type != TT_EOF)
        throw expected_tokens(token, { Term::from_character.find(eof)->second });
}

void LLVMParser::grammar_block()
{
    grammar_variable_declaration_list();
    if (symbols.scope_name().empty())
        grammar_callable_declaration_list();
    size_t label_next = get_next_label();
    if (symbols.scope_name().empty())
        get_current_out() << "define i32 @main() {" << std::endl;
    grammar_statement_list(label_next);
    llvm_write_label(label_next);
    if (symbols.scope_name().empty())
        get_current_out() << "ret i32 0" << std::endl
                          << "}" << std::endl;
}

void LLVMParser::grammar_variable_declaration_list()
{
    auto token = lexer.peekNext();
    auto eof = std::char_traits<char>::eof();
    if (token.term.type != TT_ID && token.term.type != TT_EOF)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" },
                                         Term::from_character.find(eof)->second });
    if (token.term.payload == "variable") {
        grammar_variable_declaration();
        token = lexer.getNext();
        if (token.term.type != TT_SEMICOLON)
            throw expected_tokens(token, { Term::from_character.find(';')->second });
        grammar_variable_declaration_list();
    }
}

void LLVMParser::grammar_variable_declaration()
{
    auto token = lexer.getNext();
    if (token.term.type != TT_ID || token.term.payload != "variable")
        throw expected_tokens(token, { { TT_ID, "variable" } });
    auto name_token = lexer.getNext();
    if (name_token.term.type != TT_ID)
        throw expected_tokens(name_token, { { TT_ID, "(arbitrary)" } });
    token = lexer.getNext();
    if (token.term.type != TT_COLON)
        throw expected_tokens(token, { Term::from_character.find(':')->second });
    DataType type = grammar_type();
    if (!symbols.insert(name_token.term.payload,
            { ST_VARIABLE, { type }, 0 }))
        throw multiply_defined_variable(name_token);
    if (symbols.scope_name().empty()) {
        // Prefix symbol names, so that we don't clash with printf:
        get_current_out() << "@GLOBAL_" << name_token.term.payload << " = global ";
        if (type == DT_INTEGER)
            get_current_out() << "i32 0, align 4" << std::endl;
        else
            get_current_out() << "double 0.000000e+00, align 8" << std::endl;
    } else {
        get_current_out() << "%" << name_token.term.payload << " = alloca ";
        if (type == DT_INTEGER)
            get_current_out() << "i32, align 4" << std::endl
                              << "store i32 0, i32* %"
                              << name_token.term.payload << ", align 4" << std::endl;
        else
            get_current_out() << "double, align 8" << std::endl
                              << "store double 0.000000e+00, double* %"
                              << name_token.term.payload << ", align 8" << std::endl;
    }
}

DataType LLVMParser::grammar_type()
{
    auto token = lexer.getNext();
    if (token.term.type != TT_ID
        || !(token.term.payload == "integer" || token.term.payload == "double"))
        throw expected_tokens(token, { { TT_ID, "integer" }, { TT_ID, "double" } });
    if (token.term.payload == "integer")
        return DT_INTEGER;
    else
        return DT_DOUBLE;
}

void LLVMParser::grammar_callable_declaration_list()
{
    auto token = lexer.peekNext();
    auto eof = std::char_traits<char>::eof();
    if (token.term.type != TT_ID && token.term.type != TT_EOF)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" },
                                         Term::from_character.find(eof)->second });
    if (token.term.payload == "function") {
        grammar_callable_declaration();
        token = lexer.getNext();
        if (token.term.type != TT_SEMICOLON)
            throw expected_tokens(token, { Term::from_character.find(';')->second });
        grammar_callable_declaration_list();
    }
}

void LLVMParser::grammar_callable_declaration()
{
    auto token = lexer.getNext();
    if (token.term.type != TT_ID || token.term.payload != "function")
        throw expected_tokens(token, { { TT_ID, "function" } });
    auto name_token = lexer.getNext();
    if (name_token.term.type != TT_ID)
        throw expected_tokens(name_token, { { TT_ID, "(arbitrary)" } });
    token = lexer.getNext();
    if (token.term.type != TT_LEFTPAREN)
        throw expected_tokens(token, { Term::from_character.find('(')->second });
    if (!symbols.insert(name_token.term.payload,
            { ST_FUNCTION, {}, 0 }))
        throw multiply_defined_function(name_token);
    std::vector<DataType>& parameter_types = symbols.find(name_token.term.payload).second.data_type;
    std::vector<std::pair<std::string, size_t> > parameter_storage_offsets;
    symbols.push(name_token.term.payload);
    start_buffering();
    grammar_parameter_list(parameter_types, parameter_storage_offsets);
    std::ostringstream out_buffer = stop_buffering();
    token = lexer.getNext();
    if (token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { Term::from_character.find(')')->second });
    token = lexer.getNext();
    if (token.term.type != TT_COLON)
        throw expected_tokens(token, { Term::from_character.find(':')->second });
    DataType return_type = grammar_type();
    assert(symbols.insert("metadata::return_data_type", { ST_METADATA, { return_type }, 0 }));
    parameter_types.push_back(return_type);
    get_current_out() << "define ";
    if (return_type == DT_INTEGER)
        get_current_out() << "i32";
    else
        get_current_out() << "double";
    // Prefix symbol names, so that we don't clash with printf:
    get_current_out() << " @GLOBAL_" << name_token.term.payload
                      << "(" << out_buffer.str() << ") {" << std::endl;
    auto parameter_type_iter = parameter_types.begin();
    auto parameter_storage_offset_iter = parameter_storage_offsets.begin();
    while (parameter_storage_offset_iter != parameter_storage_offsets.end()) {
        if (*parameter_type_iter == DT_INTEGER)
            get_current_out() << "%" << parameter_storage_offset_iter->first
                              << " = alloca i32, align 4" << std::endl
                              << "store i32 %identifier." << parameter_storage_offset_iter->second
                              << ", i32* %" << parameter_storage_offset_iter->first
                              << ", align 4" << std::endl;
        else
            get_current_out() << "%" << parameter_storage_offset_iter->first
                              << " = alloca double, align 8" << std::endl
                              << "store double %identifier." << parameter_storage_offset_iter->second
                              << ", double* %" << parameter_storage_offset_iter->first
                              << ", align 8" << std::endl;
        ++parameter_storage_offset_iter;
        ++parameter_type_iter;
    }
    grammar_block();
    if (return_type == DT_INTEGER)
        get_current_out() << "ret i32 0";
    else
        get_current_out() << "ret double 0.000000e+00";
    get_current_out() << std::endl
                      << "}" << std::endl;
    symbols.pop();
    token = lexer.getNext();
    if (token.term.type != TT_ID || token.term.payload != "end")
        throw expected_tokens(token, { { TT_ID, "end" } });
}

void LLVMParser::grammar_parameter_list(
    std::vector<DataType>& parameter_types,
    std::vector<std::pair<std::string, size_t> >& parameter_names)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" },
                                         Term::from_character.find(')')->second });
    if (token.term.type == TT_ID) {
        grammar_parameter(parameter_types, parameter_names);
        grammar_parameter_list_tail(parameter_types, parameter_names);
    }
}

void LLVMParser::grammar_parameter_list_tail(
    std::vector<DataType>& parameter_types,
    std::vector<std::pair<std::string, size_t> >& parameter_names)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_COMMA && token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { Term::from_character.find(',')->second,
                                         Term::from_character.find(')')->second });
    if (token.term.type == TT_COMMA) {
        get_current_out() << ", ";
        lexer.getNext();
        grammar_parameter(parameter_types, parameter_names);
        grammar_parameter_list_tail(parameter_types, parameter_names);
    }
}

void LLVMParser::grammar_parameter(
    std::vector<DataType>& parameter_types,
    std::vector<std::pair<std::string, size_t> >& parameter_names)
{
    auto name_token = lexer.getNext();
    if (name_token.term.type != TT_ID)
        throw expected_tokens(name_token, { { TT_ID, "(arbitrary)" } });
    auto token = lexer.getNext();
    if (token.term.type != TT_COLON)
        throw expected_tokens(token, { Term::from_character.find(':')->second });
    DataType type = grammar_type();
    if (!symbols.insert(name_token.term.payload,
            { ST_PARAMETER, { type }, 0 }))
        throw multiply_defined_variable(name_token);
    size_t storage_offset = get_next_offset();
    if (type == DT_INTEGER)
        get_current_out() << "i32";
    else
        get_current_out() << "double";
    get_current_out() << " %identifier." << storage_offset;
    parameter_types.push_back(type);
    parameter_names.push_back({ name_token.term.payload, storage_offset });
}

void LLVMParser::grammar_statement_list(size_t label_next)
{
    auto token = lexer.peekNext();
    auto eof = std::char_traits<char>::eof();
    if (token.term.type != TT_ID && token.term.type != TT_EOF)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" },
                                         Term::from_character.find(eof)->second });
    if ((token.term.type != TT_ID
            || !(token.term.payload == "end" || token.term.payload == "fi"
                   || token.term.payload == "else" || token.term.payload == "done"))
        && token.term.type != TT_EOF) {
        grammar_statement();
        token = lexer.getNext();
        if (token.term.type != TT_SEMICOLON)
            throw expected_tokens(token, { Term::from_character.find(';')->second });
        grammar_statement_list(label_next);
    } else {
        llvm_goto(label_next);
    }
}

void LLVMParser::grammar_statement()
{
    auto token = lexer.getNext();
    if (token.term.type != TT_ID)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" } });
    if (token.term.payload == "if") {
        size_t label_nested_next = get_next_label();
        size_t label_true = get_next_label();
        size_t label_false = get_next_label();
        grammar_bool_expression(label_true, label_false);
        token = lexer.getNext();
        if (token.term.type != TT_ID && token.term.payload != "then")
            throw expected_tokens(token, { { TT_ID, "then" } });
        llvm_write_label(label_true);
        grammar_statement_list(label_nested_next);
        llvm_write_label(label_false);
        grammar_statement_if_tail(label_nested_next);
        llvm_goto(label_nested_next);
        llvm_write_label(label_nested_next);
    } else if (token.term.payload == "while") {
        size_t label_nested_next = get_next_label();
        size_t label_true = get_next_label();
        size_t label_loop = get_next_label();
        llvm_goto(label_loop);
        llvm_write_label(label_loop);
        grammar_bool_expression(label_true, label_nested_next);
        token = lexer.getNext();
        if (token.term.type != TT_ID && token.term.payload != "do")
            throw expected_tokens(token, { { TT_ID, "do" } });
        llvm_write_label(label_true);
        grammar_statement_list(label_loop);
        token = lexer.getNext();
        if (token.term.type != TT_ID && token.term.payload != "done")
            throw expected_tokens(token, { { TT_ID, "done" } });
        llvm_goto(label_nested_next);
        llvm_write_label(label_nested_next);
    } else if (token.term.payload == "return") {
        SymbolData& data = symbols.find("metadata::return_data_type").second;
        assert(data.data_type.size() == 1);
        DataType expected_type = data.data_type.back();
        size_t storage_offset;
        DataType actual_type = grammar_arithmetic_expression(storage_offset);
        DataType coerced_type = return_data_type_coerce(
            token, expected_type, actual_type, storage_offset);
        size_t loaded_storage_offset = llvm_load(storage_offset, coerced_type);
        if (!symbols.scope_name().empty()) {
            get_current_out() << "ret ";
            if (coerced_type == DT_INTEGER)
                get_current_out() << "i32";
            else
                get_current_out() << "double";
            get_current_out() << " %identifier." << loaded_storage_offset << std::endl;
            get_next_offset();
        } else { // Print the returned value, when we are in the global scope.
            get_current_out() << "call i32 (i8*, ...) @printf(i8* getelementptr inbounds (";
            if (coerced_type == DT_INTEGER)
                get_current_out() << "[4 x i8], [4 x i8]* @.print_integer, i32 0, i32 0), i32 %identifier.";
            else
                get_current_out() << "[5 x i8], [5 x i8]* @.print_double, i32 0, i32 0), double %identifier.";
            get_current_out() << loaded_storage_offset << ")" << std::endl;
            get_next_offset();
            get_current_out() << "ret i32 0" << std::endl;
            get_next_offset();
        }
    } else { // Variable assignment
        auto name_token = token;
        SymbolData data;
        bool is_identifier_global;
        std::tie(is_identifier_global, data) = symbols.find(token.term.payload);
        if (data.symbol_type != ST_VARIABLE && data.symbol_type != ST_PARAMETER)
            throw undefined_variable(token);
        assert(data.data_type.size() == 1);
        DataType expected_type = data.data_type.back();
        token = lexer.getNext();
        if (token.term.type != TT_RELOP_EQ)
            throw expected_tokens(token, { Term::from_character.find('=')->second });
        size_t storage_offset;
        DataType actual_type = grammar_arithmetic_expression(storage_offset);
        DataType coerced_type = return_data_type_coerce(
            token, expected_type, actual_type, storage_offset);
        size_t loaded_storage_offset = llvm_load(storage_offset, coerced_type);
        get_current_out() << "store ";
        if (coerced_type == DT_INTEGER)
            get_current_out() << "i32 %identifier." << loaded_storage_offset << ", i32* ";
        else
            get_current_out() << "double %identifier." << loaded_storage_offset
                              << ", double* ";
        if (is_identifier_global)
            get_current_out() << "@GLOBAL_" << name_token.term.payload;
        else
            get_current_out() << "%" << name_token.term.payload;
        get_current_out() << ", align ";
        if (coerced_type == DT_INTEGER)
            get_current_out() << 4;
        else
            get_current_out() << 8;
        get_current_out() << std::endl;
    }
}

void LLVMParser::grammar_statement_if_tail(size_t label_next)
{
    auto token = lexer.getNext();
    if (token.term.type != TT_ID || !(token.term.payload == "fi" || token.term.payload == "else"))
        throw expected_tokens(token, { { TT_ID, "fi" }, { TT_ID, "else" } });
    if (token.term.payload == "else") {
        grammar_statement_list(label_next);
        token = lexer.getNext();
        if (token.term.type != TT_ID || token.term.payload != "fi")
            throw expected_tokens(token, { { TT_ID, "fi" } });
    }
}

DataType LLVMParser::grammar_arithmetic_expression(size_t& storage_offset)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" }, { TT_INTEGER, "(arbitrary)" },
                                         { TT_REAL, "(arbitrary)" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second });
    if (token.term.type == TT_ID && token.term.payload == "call") {
        lexer.getNext();
        auto name_token = lexer.getNext();
        if (name_token.term.type != TT_ID)
            throw expected_tokens(name_token, { { TT_ID, "(arbitrary)" } });
        token = lexer.getNext();
        if (token.term.type != TT_LEFTPAREN)
            throw expected_tokens(token, { Term::from_character.find('(')->second });
        SymbolData& data = symbols.find(name_token.term.payload).second;
        if (data.symbol_type != ST_FUNCTION)
            throw undefined_function(name_token);
        assert(data.data_type.size() > 0);
        // Pass a copy of the expected types reversed, so that we can pop_back() on them.
        std::vector<DataType> expected_types;
        expected_types.resize(data.data_type.size() - 1);
        std::reverse_copy(data.data_type.begin(),
            data.data_type.end() - 1, expected_types.begin());
        std::vector<size_t> parameter_storage_offsets;
        grammar_arithmetic_expression_list(expected_types, parameter_storage_offsets);
        token = lexer.getNext();
        if (token.term.type != TT_RIGHTPAREN)
            throw expected_tokens(token, { Term::from_character.find(')')->second });
        size_t loaded_storage_offset = get_next_offset();
        DataType return_type = data.data_type.back();
        get_current_out() << "%identifier." << loaded_storage_offset << " = call ";
        if (return_type == DT_INTEGER)
            get_current_out() << "i32";
        else
            get_current_out() << "double";
        get_current_out() << " @GLOBAL_" << name_token.term.payload << "(";
        auto parameter_type_iter = data.data_type.begin();
        auto parameter_storage_offset_iter = parameter_storage_offsets.begin();
        while (
            parameter_storage_offset_iter != parameter_storage_offsets.end()) {
            if (*parameter_type_iter == DT_INTEGER)
                get_current_out() << "i32";
            else
                get_current_out() << "double";
            get_current_out() << " %identifier." << *parameter_storage_offset_iter;
            ++parameter_storage_offset_iter;
            ++parameter_type_iter;
            if (parameter_storage_offset_iter != parameter_storage_offsets.end())
                get_current_out() << ", ";
        }
        get_current_out() << ")" << std::endl;
        storage_offset = llvm_store(loaded_storage_offset, return_type);
        return return_type;
    } else {
        size_t left_operand_storage_offset;
        DataType left_operand_type = grammar_arithmetic_factor(left_operand_storage_offset);
        return grammar_arithmetic_expression_tail(left_operand_type,
            left_operand_storage_offset, storage_offset);
    }
}

DataType LLVMParser::grammar_arithmetic_expression_tail(DataType left_operand_type,
    size_t left_operand_storage_offset, size_t& storage_offset)
{
    auto token = lexer.peekNext();
    if ((token.term.type != TT_ID
            || !(token.term.payload == "end" || token.term.payload == "and"
                   || token.term.payload == "or" || token.term.payload == "then"
                   || token.term.payload == "do"))
        && token.term.type != TT_ARITHOP_PLUS
        && token.term.type != TT_COMMA && token.term.type != TT_RIGHTPAREN
        && token.term.type != TT_RELOP_LT && token.term.type != TT_RELOP_EQ
        && token.term.type != TT_RELOP_GT && token.term.type != TT_SEMICOLON
        && token.term.type != TT_RIGHTBRACKET)
        throw expected_tokens(token, { Term::from_character.find('+')->second,
                                         Term::from_character.find('-')->second,
                                         Term::from_character.find(',')->second,
                                         Term::from_character.find(')')->second,
                                         Term::from_character.find('<')->second,
                                         Term::from_character.find('=')->second,
                                         Term::from_character.find('>')->second,
                                         Term::from_character.find(';')->second,
                                         { TT_ID, "end" }, { TT_ID, "and" }, { TT_ID, "or" },
                                         Term::from_character.find(']')->second,
                                         { TT_ID, "then" }, { TT_ID, "do" } });
    if (token.term.type == TT_ARITHOP_PLUS) {
        lexer.getNext();
        size_t right_operand_storage_offset;
        DataType right_operand_type = grammar_arithmetic_expression(right_operand_storage_offset);
        DataType coerced_type = arithmetic_binary_operator_coerce(
            left_operand_type, right_operand_type,
            left_operand_storage_offset, right_operand_storage_offset);
        size_t loaded_left_operand_storage_offset = llvm_load(
            left_operand_storage_offset, coerced_type);
        size_t loaded_right_operand_storage_offset = llvm_load(
            right_operand_storage_offset, coerced_type);
        size_t loaded_offset = get_next_offset();
        get_current_out() << "%identifier." << loaded_offset << " = ";
        if (coerced_type == DT_INTEGER)
            get_current_out() << "add";
        else
            get_current_out() << "fadd";
        get_current_out() << " ";
        if (coerced_type == DT_INTEGER)
            get_current_out() << "i32";
        else
            get_current_out() << "double";
        get_current_out() << " %identifier." << loaded_left_operand_storage_offset
                          << ", %identifier." << loaded_right_operand_storage_offset << std::endl;
        storage_offset = llvm_store(loaded_offset, coerced_type);
        return coerced_type;
    } else {
        storage_offset = left_operand_storage_offset;
        return left_operand_type;
    }
}

void LLVMParser::grammar_arithmetic_expression_list(
    std::vector<DataType>& expected_types, std::vector<size_t>& storage_offsets)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS && token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" }, { TT_INTEGER, "(arbitrary)" },
                                         { TT_REAL, "(arbitrary)" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second,
                                         Term::from_character.find(')')->second });
    if (token.term.type != TT_RIGHTPAREN) {
        size_t storage_offset;
        DataType actual_type = grammar_arithmetic_expression(storage_offset);
        DataType expected_type = expected_types.back();
        DataType coerced_type = return_data_type_coerce(
            token, expected_type, actual_type, storage_offset);
        storage_offsets.push_back(llvm_load(storage_offset, coerced_type));
        expected_types.pop_back();
        grammar_arithmetic_expression_list_tail(expected_types, storage_offsets);
    }
}

void LLVMParser::grammar_arithmetic_expression_list_tail(
    std::vector<DataType>& expected_types, std::vector<size_t>& storage_offsets)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_COMMA && token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { Term::from_character.find(',')->second,
                                         Term::from_character.find(')')->second });
    if (token.term.type == TT_COMMA) {
        lexer.getNext();
        if (expected_types.empty())
            throw extra_parameter(lexer.peekNext());
        size_t storage_offset;
        DataType actual_type = grammar_arithmetic_expression(storage_offset);
        return_data_type_coerce(token, expected_types.back(), actual_type, storage_offset);
        DataType expected_type = expected_types.back();
        DataType coerced_type = return_data_type_coerce(
            token, expected_type, actual_type, storage_offset);
        storage_offsets.push_back(llvm_load(storage_offset, coerced_type));
        expected_types.pop_back();
        grammar_arithmetic_expression_list_tail(expected_types, storage_offsets);
    } else {
        if (!expected_types.empty())
            throw missing_parameter(token);
    }
}

DataType LLVMParser::grammar_arithmetic_factor(size_t& storage_offset)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" }, { TT_INTEGER, "(arbitrary)" },
                                         { TT_REAL, "(arbitrary)" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second });
    size_t left_operand_storage_offset;
    DataType left_operand_type = grammar_arithmetic_operand(left_operand_storage_offset);
    return grammar_arithmetic_factor_tail(left_operand_type,
        left_operand_storage_offset, storage_offset);
}

DataType LLVMParser::grammar_arithmetic_factor_tail(DataType left_operand_type,
    size_t left_operand_storage_offset, size_t& storage_offset)
{
    auto token = lexer.peekNext();
    if ((token.term.type != TT_ID
            || !(token.term.payload == "mod" || token.term.payload == "end"
                   || token.term.payload == "and" || token.term.payload == "or"
                   || token.term.payload == "then" || token.term.payload == "do"))
        && token.term.type != TT_ARITHOP_MUL && token.term.type != TT_ARITHOP_DIV
        && token.term.type != TT_ARITHOP_PLUS && token.term.type != TT_ARITHOP_MINUS
        && token.term.type != TT_COMMA && token.term.type != TT_RIGHTPAREN
        && token.term.type != TT_RELOP_LT && token.term.type != TT_RELOP_EQ
        && token.term.type != TT_RELOP_GT && token.term.type != TT_SEMICOLON
        && token.term.type != TT_RIGHTBRACKET)
        throw expected_tokens(token, { Term::from_character.find('*')->second,
                                         Term::from_character.find('/')->second,
                                         { TT_ID, "mod" },
                                         Term::from_character.find('+')->second,
                                         Term::from_character.find('-')->second,
                                         Term::from_character.find(',')->second,
                                         Term::from_character.find(')')->second,
                                         Term::from_character.find('<')->second,
                                         Term::from_character.find('=')->second,
                                         Term::from_character.find('>')->second,
                                         Term::from_character.find(';')->second,
                                         { TT_ID, "end" }, { TT_ID, "and" }, { TT_ID, "or" },
                                         Term::from_character.find(']')->second,
                                         { TT_ID, "then" }, { TT_ID, "do" } });
    if (token.term.type == TT_ARITHOP_MUL || token.term.type == TT_ARITHOP_DIV
        || (token.term.type == TT_ID && token.term.payload == "mod")) {
        lexer.getNext();
        size_t right_operand_storage_offset;
        DataType right_operand_type = grammar_arithmetic_factor(right_operand_storage_offset);
        DataType coerced_type = arithmetic_binary_operator_coerce(
            left_operand_type, right_operand_type,
            left_operand_storage_offset, right_operand_storage_offset);
        size_t loaded_left_operand_storage_offset = llvm_load(
            left_operand_storage_offset, coerced_type);
        size_t loaded_right_operand_storage_offset = llvm_load(
            right_operand_storage_offset, coerced_type);
        size_t loaded_offset = get_next_offset();
        get_current_out() << "%identifier." << loaded_offset << " = ";
        if (token.term.type == TT_ARITHOP_MUL) {
            if (coerced_type == DT_INTEGER)
                get_current_out() << "mul";
            else
                get_current_out() << "fmul";
        } else if (token.term.type == TT_ARITHOP_DIV) {
            if (coerced_type == DT_INTEGER)
                get_current_out() << "sdiv";
            else
                get_current_out() << "fdiv";
        } else {
            if (coerced_type == DT_INTEGER)
                get_current_out()
                    << "srem";
            else
                get_current_out() << "frem";
        }
        get_current_out() << " ";
        if (coerced_type == DT_INTEGER)
            get_current_out() << "i32";
        else
            get_current_out() << "double";
        get_current_out() << " %identifier." << loaded_left_operand_storage_offset
                          << ", %identifier." << loaded_right_operand_storage_offset << std::endl;
        storage_offset = llvm_store(loaded_offset, coerced_type);
        return coerced_type;
    } else {
        storage_offset = left_operand_storage_offset;
        return left_operand_type;
    }
}

DataType LLVMParser::grammar_arithmetic_operand(size_t& storage_offset)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS)
        throw expected_tokens(token, { { TT_ID, "arbitrary" },
                                         { TT_INTEGER, "arbitrary" }, { TT_REAL, "arbitrary" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second });
    lexer.getNext();
    if (token.term.type == TT_LEFTPAREN) {
        DataType type = grammar_arithmetic_expression(storage_offset);
        token = lexer.getNext();
        if (token.term.type != TT_RIGHTPAREN)
            throw expected_tokens(token, { Term::from_character.find(')')->second });
        return type;
    } else if (token.term.type == TT_ARITHOP_MINUS) {
        size_t nested_storage_offset;
        DataType type = grammar_arithmetic_operand(nested_storage_offset);
        size_t loaded_nested_storage_offset = llvm_load(nested_storage_offset, type);
        size_t loaded_storage_offset = get_next_offset();
        if (type == DT_INTEGER)
            get_current_out() << "%identifier." << loaded_storage_offset
                              << " = sub i32 0, %identifier." << loaded_nested_storage_offset << std::endl;
        else
            get_current_out() << "%identifier." << loaded_storage_offset
                              << " = fsub double -0.000000e+00, %identifier."
                              << loaded_nested_storage_offset << std::endl;
        storage_offset = llvm_store(loaded_storage_offset, type);
        return type;
    } else if (token.term.type == TT_ID) {
        SymbolData data;
        bool is_identifier_global;
        std::tie(is_identifier_global, data) = symbols.find(token.term.payload);
        size_t loaded_storage_offset = get_next_offset();
        if (data.symbol_type != ST_VARIABLE && data.symbol_type != ST_PARAMETER)
            throw undefined_variable(token);
        assert(data.data_type.size() == 1);
        DataType type = data.data_type.back();
        get_current_out() << "%identifier." << loaded_storage_offset << " = load ";
        if (type == DT_INTEGER)
            get_current_out() << "i32, i32*";
        else
            get_current_out() << "double, double*";
        get_current_out() << " ";
        if (is_identifier_global)
            get_current_out() << "@GLOBAL_";
        else
            get_current_out() << "%";
        get_current_out() << token.term.payload << ", align 4" << std::endl;
        storage_offset = llvm_store(loaded_storage_offset, type);
        return type;
    } else if (token.term.type == TT_INTEGER) {
        storage_offset = get_next_offset();
        get_current_out() << "%identifier." << storage_offset
                          << " = alloca i32, align 4" << std::endl
                          << "store i32 " << token.term.payload
                          << ", i32* %identifier." << storage_offset << ", align 4" << std::endl;
        return DT_INTEGER;
    } else if (token.term.type == TT_REAL) {
        storage_offset = get_next_offset();
        get_current_out() << "%identifier." << storage_offset
                          << " = alloca double, align 8" << std::endl
                          << "store double " << token.term.payload
                          << ", double* %identifier." << storage_offset << ", align 8" << std::endl;
        return DT_DOUBLE;
    }
    assert(false); // Should never reach here.
    return DT_ARBITRARY;
}

void LLVMParser::grammar_bool_expression(
    size_t label_true, size_t label_false)
{
    size_t label_nested_false = get_next_label();
    grammar_bool_factor(label_true, label_nested_false);
    grammar_bool_expression_tail(label_true, label_false, label_nested_false);
}

void LLVMParser::grammar_bool_expression_tail(
    size_t label_true,
    size_t label_false, size_t label_nested_false)
{
    auto token = lexer.peekNext();
    if ((token.term.type != TT_ID
            || !(token.term.payload == "or" || token.term.payload == "then"
                   || token.term.payload == "do"))
        && token.term.type != TT_RIGHTBRACKET)
        throw expected_tokens(token, { { TT_ID, "or" }, { TT_ID, "then" },
                                         { TT_ID, "do" },
                                         Term::from_character.find(']')->second });
    if (token.term.type == TT_ID && token.term.payload == "or") {
        lexer.getNext();
        llvm_write_label(label_nested_false);
        grammar_bool_expression(label_true, label_false);
    } else {
        llvm_write_label(label_nested_false);
        llvm_goto(label_false);
    }
}

void LLVMParser::grammar_bool_factor(
    size_t label_true, size_t label_false)
{
    size_t label_nested_true = get_next_label();
    grammar_bool_operand(label_nested_true, label_false);
    grammar_bool_factor_tail(label_true, label_nested_true, label_false);
}

void LLVMParser::grammar_bool_factor_tail(
    size_t label_true, size_t label_nested_true,
    size_t label_false)
{
    auto token = lexer.peekNext();
    if ((token.term.type != TT_ID
            || !(token.term.payload == "and" || token.term.payload == "or"
                   || token.term.payload == "then" || token.term.payload == "do"))
        && token.term.type != TT_RIGHTBRACKET)
        throw expected_tokens(token, { { TT_ID, "and" }, { TT_ID, "or" },
                                         { TT_ID, "then" }, { TT_ID, "do" },
                                         Term::from_character.find(']')->second });
    if (token.term.type == TT_ID && token.term.payload == "and") {
        lexer.getNext();
        llvm_write_label(label_nested_true);
        grammar_bool_factor(label_true, label_false);
    } else {
        llvm_write_label(label_nested_true);
        llvm_goto(label_true);
    }
}

void LLVMParser::grammar_bool_operand(
    size_t label_true, size_t label_false)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS && token.term.type != TT_LEFTBRACKET)
        throw expected_tokens(token, { { TT_ID, "arbitrary" },
                                         { TT_INTEGER, "arbitrary" }, { TT_REAL, "arbitrary" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second,
                                         Term::from_character.find('[')->second });
    if (token.term.type == TT_LEFTBRACKET) {
        lexer.getNext();
        grammar_bool_expression(label_true, label_false);
        token = lexer.getNext();
        if (token.term.type != TT_RIGHTBRACKET)
            throw expected_tokens(token, { Term::from_character.find(']')->second });
    } else if (token.term.type == TT_ID && token.term.payload == "not") {
        lexer.getNext();
        grammar_bool_operand(label_false, label_true);
    } else if (token.term.type == TT_ID
        && (token.term.payload == "true" || token.term.payload == "false")) {
        lexer.getNext();
        if (token.term.payload == "true")
            llvm_goto(label_true);
        else
            llvm_goto(label_false);
    } else {
        size_t loaded_storage_offset = grammar_relation_expression();
        llvm_conditional_goto(loaded_storage_offset, label_false, label_true);
    }
}

size_t LLVMParser::grammar_relation_expression()
{
    size_t left_operand_storage_offset;
    DataType left_operand_type = grammar_arithmetic_expression(left_operand_storage_offset);
    return grammar_relation_expression_tail(left_operand_storage_offset, left_operand_type);
}

size_t LLVMParser::grammar_relation_expression_tail(
    size_t left_operand_storage_offset, DataType left_operand_type)
{
    auto token = lexer.getNext();
    if (token.term.type != TT_RELOP_LT && token.term.type != TT_RELOP_EQ
        && token.term.type != TT_RELOP_GT)
        throw expected_tokens(token, { Term::from_character.find('<')->second,
                                         Term::from_character.find('=')->second,
                                         Term::from_character.find('>')->second });
    size_t right_operand_storage_offset;
    DataType right_operand_type = grammar_arithmetic_expression(right_operand_storage_offset);
    DataType coerced_type = arithmetic_binary_operator_coerce(
        left_operand_type, right_operand_type,
        left_operand_storage_offset, right_operand_storage_offset);
    size_t loaded_left_operand_storage_offset = llvm_load(
        left_operand_storage_offset, coerced_type);
    size_t loaded_right_operand_storage_offset = llvm_load(
        right_operand_storage_offset, coerced_type);
    size_t loaded_storage_offset = get_next_offset();
    get_current_out() << "%identifier." << loaded_storage_offset << " = ";
    if (coerced_type == DT_INTEGER) {
        get_current_out() << "icmp ";
        switch (token.term.type) {
        case TT_RELOP_LT:
            get_current_out() << "slt";
            break;
        case TT_RELOP_EQ:
            get_current_out() << "eq";
            break;
        case TT_RELOP_GT:
            get_current_out() << "sgt";
            break;
        default:
            assert(false); // Should never reach here.
        }
        get_current_out() << " i32 %identifier." << loaded_left_operand_storage_offset
                          << ", %identifier." << loaded_right_operand_storage_offset << std::endl;
    } else {
        get_current_out() << "fcmp ";
        switch (token.term.type) {
        case TT_RELOP_LT:
            get_current_out() << "olt";
            break;
        case TT_RELOP_EQ:
            get_current_out() << "oeq";
            break;
        case TT_RELOP_GT:
            get_current_out() << "ogt";
            break;
        default:
            assert(false); // Should never reach here.
        }
        get_current_out() << " double %identifier." << loaded_left_operand_storage_offset
                          << ", %identifier." << loaded_right_operand_storage_offset << std::endl;
    }
    return loaded_storage_offset;
}
