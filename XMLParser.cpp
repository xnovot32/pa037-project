#include "XMLParser.hpp"
#include "Lexer.hpp"
#include <assert.h>
#include <iostream>

DataType XMLParser::arithmetic_binary_operator_coerce(
    DataType left_operand_type, DataType right_operand_type)
{
    assert(left_operand_type != DT_ARBITRARY);
    assert(right_operand_type != DT_ARBITRARY);
    if (left_operand_type == DT_INTEGER && right_operand_type == DT_INTEGER)
        return DT_INTEGER;
    else
        return DT_DOUBLE;
}

DataType XMLParser::return_data_type_coerce(
    const Token& where, DataType expected_type, DataType actual_type)
{
    assert(actual_type != DT_ARBITRARY);
    if (expected_type == DT_ARBITRARY)
        return actual_type;
    if (expected_type == actual_type || (expected_type == DT_DOUBLE && actual_type == DT_INTEGER))
        return expected_type;
    else if (expected_type == DT_INTEGER && actual_type == DT_DOUBLE)
        throw cannot_coerce(where, "double", "integer");
    assert(false); // Should never reach here.
}

void XMLParser::run()
{
    grammar_init();
}

void XMLParser::grammar_init()
{
    symbols.push("");
    assert(symbols.insert("metadata::return_data_type", { ST_METADATA, { DT_ARBITRARY }, 0 }));
    assert(symbols.insert("metadata::current_offset", { ST_METADATA, { DT_ARBITRARY }, 0 }));
    grammar_block();
    symbols.pop();
    auto token = lexer.getNext();
    auto eof = std::char_traits<char>::eof();
    if (token.term.type != TT_EOF)
        throw expected_tokens(token, { Term::from_character.find(eof)->second });
}

void XMLParser::grammar_block()
{
    get_current_out() << "<block>" << std::endl;
    grammar_variable_declaration_list(false);
    if (symbols.scope_name().empty())
        grammar_callable_declaration_list(false);
    grammar_statement_list(false);
    get_current_out() << "</block>" << std::endl;
}

void XMLParser::grammar_variable_declaration_list(bool is_nested)
{
    auto token = lexer.peekNext();
    auto eof = std::char_traits<char>::eof();
    if (token.term.type != TT_ID && token.term.type != TT_EOF)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" },
                                         Term::from_character.find(eof)->second });
    if (token.term.payload == "variable") {
        if (!is_nested)
            get_current_out() << "<variables>" << std::endl;
        grammar_variable_declaration();
        token = lexer.getNext();
        if (token.term.type != TT_SEMICOLON)
            throw expected_tokens(token, { Term::from_character.find(';')->second });
        grammar_variable_declaration_list(true);
        if (!is_nested)
            get_current_out() << "</variables>" << std::endl;
    }
}

void XMLParser::grammar_variable_declaration()
{
    get_current_out() << "<variable>" << std::endl;
    auto token = lexer.getNext();
    if (token.term.type != TT_ID || token.term.payload != "variable")
        throw expected_tokens(token, { { TT_ID, "variable" } });
    auto name_token = lexer.getNext();
    if (name_token.term.type != TT_ID)
        throw expected_tokens(name_token, { { TT_ID, "(arbitrary)" } });
    get_current_out() << "<name>" << std::endl
                      << name_token.term.payload << std::endl
                      << "</name>" << std::endl;
    token = lexer.getNext();
    if (token.term.type != TT_COLON)
        throw expected_tokens(token, { Term::from_character.find(':')->second });
    DataType type = grammar_type();
    auto& offset = symbols.find("metadata::current_offset").second.offset;
    if (!symbols.insert(name_token.term.payload,
            { ST_VARIABLE, { type }, offset++ }))
        throw multiply_defined_variable(name_token);
    get_current_out() << "</variable>" << std::endl;
}

DataType XMLParser::grammar_type()
{
    get_current_out() << "<type>" << std::endl;
    auto token = lexer.getNext();
    if (token.term.type != TT_ID
        || !(token.term.payload == "integer" || token.term.payload == "double"))
        throw expected_tokens(token, { { TT_ID, "integer" }, { TT_ID, "double" } });
    get_current_out() << token.term.payload << std::endl
                      << "</type>" << std::endl;
    if (token.term.payload == "integer")
        return DT_INTEGER;
    else
        return DT_DOUBLE;
}

void XMLParser::grammar_callable_declaration_list(bool is_nested)
{
    auto token = lexer.peekNext();
    auto eof = std::char_traits<char>::eof();
    if (token.term.type != TT_ID && token.term.type != TT_EOF)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" },
                                         Term::from_character.find(eof)->second });
    if (token.term.payload == "function") {
        if (!is_nested)
            get_current_out() << "<functions>" << std::endl;
        grammar_callable_declaration();
        token = lexer.getNext();
        if (token.term.type != TT_SEMICOLON)
            throw expected_tokens(token, { Term::from_character.find(';')->second });
        grammar_callable_declaration_list(true);
        if (!is_nested)
            get_current_out() << "</functions>" << std::endl;
    }
}

void XMLParser::grammar_callable_declaration()
{
    get_current_out() << "<function>" << std::endl;
    auto token = lexer.getNext();
    if (token.term.type != TT_ID || token.term.payload != "function")
        throw expected_tokens(token, { { TT_ID, "function" } });
    auto name_token = lexer.getNext();
    if (name_token.term.type != TT_ID)
        throw expected_tokens(name_token, { { TT_ID, "(arbitrary)" } });
    get_current_out() << "<name>" << std::endl
                      << name_token.term.payload << std::endl
                      << "</name>" << std::endl;
    token = lexer.getNext();
    if (token.term.type != TT_LEFTPAREN)
        throw expected_tokens(token, { Term::from_character.find('(')->second });
    if (!symbols.insert(name_token.term.payload,
            { ST_FUNCTION, {}, 0 }))
        throw multiply_defined_function(name_token);
    std::vector<DataType>& parameter_types = symbols.find(name_token.term.payload).second.data_type;
    symbols.push(name_token.term.payload);
    grammar_parameter_list(parameter_types);
    token = lexer.getNext();
    if (token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { Term::from_character.find(')')->second });
    token = lexer.getNext();
    if (token.term.type != TT_COLON)
        throw expected_tokens(token, { Term::from_character.find(':')->second });
    DataType return_type = grammar_type();
    assert(symbols.insert("metadata::return_data_type", { ST_METADATA, { return_type }, 0 }));
    assert(symbols.insert("metadata::current_offset", { ST_METADATA, { DT_ARBITRARY }, 0 }));
    parameter_types.push_back(return_type);
    grammar_block();
    symbols.pop();
    token = lexer.getNext();
    if (token.term.type != TT_ID || token.term.payload != "end")
        throw expected_tokens(token, { { TT_ID, "end" } });
    get_current_out() << "</function>" << std::endl;
}

void XMLParser::grammar_parameter_list(std::vector<DataType>& parameter_types)
{
    get_current_out() << "<parameters>" << std::endl;
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" },
                                         Term::from_character.find(')')->second });
    if (token.term.type == TT_ID) {
        grammar_parameter(parameter_types);
        grammar_parameter_list_tail(parameter_types);
    }
}

void XMLParser::grammar_parameter_list_tail(std::vector<DataType>& parameter_types)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_COMMA && token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { Term::from_character.find(',')->second,
                                         Term::from_character.find(')')->second });
    if (token.term.type == TT_COMMA) {
        lexer.getNext();
        grammar_parameter(parameter_types);
        grammar_parameter_list_tail(parameter_types);
    } else {
        get_current_out() << "</parameters>" << std::endl;
    }
}

void XMLParser::grammar_parameter(std::vector<DataType>& parameter_types)
{
    get_current_out() << "<parameter>" << std::endl;
    auto name_token = lexer.getNext();
    if (name_token.term.type != TT_ID)
        throw expected_tokens(name_token, { { TT_ID, "(arbitrary)" } });
    get_current_out() << "<name>" << std::endl
                      << name_token.term.payload << std::endl
                      << "</name>" << std::endl;
    auto token = lexer.getNext();
    if (token.term.type != TT_COLON)
        throw expected_tokens(token, { Term::from_character.find(':')->second });
    DataType type = grammar_type();
    if (!symbols.insert(name_token.term.payload,
            { ST_PARAMETER, { type }, 0 }))
        throw multiply_defined_variable(name_token);
    parameter_types.push_back(type);
    get_current_out() << "</parameter>" << std::endl;
}

void XMLParser::grammar_statement_list(bool is_nested)
{
    auto token = lexer.peekNext();
    auto eof = std::char_traits<char>::eof();
    if (token.term.type != TT_ID && token.term.type != TT_EOF)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" },
                                         Term::from_character.find(eof)->second });
    if ((token.term.type != TT_ID
            || !(token.term.payload == "end" || token.term.payload == "fi"
                   || token.term.payload == "else" || token.term.payload == "done"))
        && token.term.type != TT_EOF) {
        if (!is_nested)
            get_current_out() << "<statements>" << std::endl;
        grammar_statement();
        token = lexer.getNext();
        if (token.term.type != TT_SEMICOLON)
            throw expected_tokens(token, { Term::from_character.find(';')->second });
        grammar_statement_list(true);
        if (!is_nested)
            get_current_out() << "</statements>" << std::endl;
    }
}

void XMLParser::grammar_statement()
{
    get_current_out() << "<statement>" << std::endl;
    auto token = lexer.getNext();
    if (token.term.type != TT_ID)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" } });
    if (token.term.payload == "if") {
        get_current_out() << "<if>" << std::endl;
        grammar_bool_expression();
        token = lexer.getNext();
        if (token.term.type != TT_ID && token.term.payload != "then")
            throw expected_tokens(token, { { TT_ID, "then" } });
        grammar_statement_list(false);
        grammar_statement_if_tail();
    } else if (token.term.payload == "while") {
        get_current_out() << "<while>" << std::endl;
        grammar_bool_expression();
        token = lexer.getNext();
        if (token.term.type != TT_ID && token.term.payload != "do")
            throw expected_tokens(token, { { TT_ID, "do" } });
        grammar_statement_list(false);
        token = lexer.getNext();
        if (token.term.type != TT_ID && token.term.payload != "done")
            throw expected_tokens(token, { { TT_ID, "done" } });
        get_current_out() << "</while>" << std::endl;
    } else if (token.term.payload == "return") {
        get_current_out() << "<return>" << std::endl;
        SymbolData& data = symbols.find("metadata::return_data_type").second;
        assert(data.data_type.size() == 1);
        DataType expected_type = data.data_type.back();
        grammar_coerced_arithmetic_expression(token, expected_type);
        get_current_out() << "</return>" << std::endl;
    } else {
        get_current_out() << "<assignment>" << std::endl
                          << "<identifier>" << std::endl
                          << token.term.payload << std::endl
                          << "</identifier>" << std::endl;
        SymbolData& data = symbols.find(token.term.payload).second;
        if (data.symbol_type != ST_VARIABLE && data.symbol_type != ST_PARAMETER)
            throw undefined_variable(token);
        assert(data.data_type.size() == 1);
        DataType expected_type = data.data_type.back();
        token = lexer.getNext();
        if (token.term.type != TT_RELOP_EQ)
            throw expected_tokens(token, { Term::from_character.find('=')->second });
        grammar_coerced_arithmetic_expression(token, expected_type);
        get_current_out() << "</assignment>" << std::endl;
    }
    get_current_out() << "</statement>" << std::endl;
}

void XMLParser::grammar_statement_if_tail()
{
    auto token = lexer.getNext();
    if (token.term.type != TT_ID || !(token.term.payload == "fi" || token.term.payload == "else"))
        throw expected_tokens(token, { { TT_ID, "fi" }, { TT_ID, "else" } });
    get_current_out() << "</if>" << std::endl;
    if (token.term.payload == "else") {
        get_current_out() << "<else>" << std::endl;
        grammar_statement_list(false);
        token = lexer.getNext();
        if (token.term.type != TT_ID || token.term.payload != "fi")
            throw expected_tokens(token, { { TT_ID, "fi" } });
        get_current_out() << "</else>" << std::endl;
    }
}

void XMLParser::grammar_coerced_arithmetic_expression(const Token& where, DataType expected_type)
{
    start_buffering();
    DataType actual_type = grammar_arithmetic_expression();
    std::ostringstream out_buffer = stop_buffering();
    DataType coerced_type = return_data_type_coerce(where, expected_type, actual_type);
    if (coerced_type != actual_type) {
        get_current_out() << "<coerce>" << std::endl
                          << "<type>" << std::endl;
        if (coerced_type == DT_INTEGER)
            get_current_out() << "integer" << std::endl;
        else
            get_current_out() << "double" << std::endl;
        get_current_out() << "</type>" << std::endl
                          << "<arithmetic_expression>" << std::endl
                          << out_buffer.str()
                          << "</arithmetic_expression>" << std::endl
                          << "</coerce>" << std::endl;
    } else {
        get_current_out() << out_buffer.str();
    }
}

DataType XMLParser::grammar_arithmetic_expression()
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" }, { TT_INTEGER, "(arbitrary)" },
                                         { TT_REAL, "(arbitrary)" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second });
    if (token.term.type == TT_ID && token.term.payload == "call") {
        lexer.getNext();
        auto name_token = lexer.getNext();
        if (name_token.term.type != TT_ID)
            throw expected_tokens(name_token, { { TT_ID, "(arbitrary)" } });
        get_current_out() << "<call>" << std::endl
                          << "<identifier>" << std::endl
                          << name_token.term.payload << std::endl
                          << "</identifier>" << std::endl;
        token = lexer.getNext();
        if (token.term.type != TT_LEFTPAREN)
            throw expected_tokens(token, { Term::from_character.find('(')->second });
        SymbolData& data = symbols.find(name_token.term.payload).second;
        if (data.symbol_type != ST_FUNCTION)
            throw undefined_function(name_token);
        assert(data.data_type.size() > 0);
        // Pass a copy of the expected types reversed, so that we can pop_back() on them.
        std::vector<DataType> expected_types;
        expected_types.resize(data.data_type.size() - 1);
        std::reverse_copy(data.data_type.begin(),
            data.data_type.end() - 1, expected_types.begin());
        grammar_arithmetic_expression_list(expected_types);
        token = lexer.getNext();
        if (token.term.type != TT_RIGHTPAREN)
            throw expected_tokens(token, { Term::from_character.find(')')->second });
        get_current_out() << "</call>" << std::endl;
        return data.data_type.back();
    } else {
        start_buffering();
        DataType left_operand_type = grammar_arithmetic_factor();
        return grammar_arithmetic_expression_tail(left_operand_type);
    }
}

DataType XMLParser::grammar_arithmetic_expression_tail(DataType left_operand_type)
{
    auto token = lexer.peekNext();
    if ((token.term.type != TT_ID
            || !(token.term.payload == "end" || token.term.payload == "and"
                   || token.term.payload == "or" || token.term.payload == "then"
                   || token.term.payload == "do"))
        && token.term.type != TT_ARITHOP_PLUS && token.term.type != TT_ARITHOP_MINUS
        && token.term.type != TT_COMMA && token.term.type != TT_RIGHTPAREN
        && token.term.type != TT_RELOP_LT && token.term.type != TT_RELOP_EQ
        && token.term.type != TT_RELOP_GT && token.term.type != TT_SEMICOLON
        && token.term.type != TT_RIGHTBRACKET)
        throw expected_tokens(token, { Term::from_character.find('+')->second,
                                         Term::from_character.find(',')->second,
                                         Term::from_character.find(')')->second,
                                         Term::from_character.find('<')->second,
                                         Term::from_character.find('=')->second,
                                         Term::from_character.find('>')->second,
                                         Term::from_character.find(';')->second,
                                         { TT_ID, "end" }, { TT_ID, "and" }, { TT_ID, "or" },
                                         Term::from_character.find(']')->second,
                                         { TT_ID, "then" }, { TT_ID, "do" } });
    std::ostringstream out_buffer = stop_buffering();
    if (token.term.type == TT_ARITHOP_PLUS) {
        get_current_out() << "<arithmetic_binary_operator_addition>" << std::endl
                          << "<left_operand>" << std::endl
                          << out_buffer.str()
                          << "</left_operand>" << std::endl
                          << "<right_operand>" << std::endl;
        lexer.getNext();
        DataType right_operand_type = grammar_arithmetic_expression();
        get_current_out() << "</right_operand>" << std::endl
                          << "</arithmetic_binary_operator_addition>" << std::endl
                          << "<operand_coerce>" << std::endl;
        DataType coerced_type = arithmetic_binary_operator_coerce(
            left_operand_type, right_operand_type);
        if (coerced_type == DT_INTEGER)
            get_current_out() << "integer" << std::endl;
        else
            get_current_out() << "double" << std::endl;
        get_current_out() << "</operand_coerce>" << std::endl;
        return coerced_type;
    } else {
        // Avoid unnecessarily nested output if there is no nesting.
        get_current_out() << out_buffer.str();
        return left_operand_type;
    }
}

void XMLParser::grammar_arithmetic_expression_list(std::vector<DataType>& expected_types)
{
    get_current_out() << "<arithmetic_expressions>" << std::endl;
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS && token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" }, { TT_INTEGER, "(arbitrary)" },
                                         { TT_REAL, "(arbitrary)" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second,
                                         Term::from_character.find(')')->second });
    if (token.term.type != TT_RIGHTPAREN) {
        get_current_out() << "<arithmetic_expression>" << std::endl;
        grammar_coerced_arithmetic_expression(token, expected_types.back());
        get_current_out() << "</arithmetic_expression>" << std::endl;
        expected_types.pop_back();
        grammar_arithmetic_expression_list_tail(expected_types);
    }
    get_current_out() << "</arithmetic_expressions>" << std::endl;
}

void XMLParser::grammar_arithmetic_expression_list_tail(std::vector<DataType>& expected_types)
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_COMMA && token.term.type != TT_RIGHTPAREN)
        throw expected_tokens(token, { Term::from_character.find(',')->second,
                                         Term::from_character.find(')')->second });
    if (token.term.type == TT_COMMA) {
        lexer.getNext();
        if (expected_types.empty())
            throw extra_parameter(lexer.peekNext());
        get_current_out() << "<arithmetic_expression>" << std::endl;
        grammar_coerced_arithmetic_expression(token, expected_types.back());
        get_current_out() << "</arithmetic_expression>" << std::endl;
        expected_types.pop_back();
        grammar_arithmetic_expression_list_tail(expected_types);
    } else {
        if (!expected_types.empty())
            throw missing_parameter(token);
    }
}

DataType XMLParser::grammar_arithmetic_factor()
{
    start_buffering();
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS)
        throw expected_tokens(token, { { TT_ID, "(arbitrary)" }, { TT_INTEGER, "(arbitrary)" },
                                         { TT_REAL, "(arbitrary)" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second });
    DataType left_operand_type = grammar_arithmetic_operand();
    return grammar_arithmetic_factor_tail(left_operand_type);
}

DataType XMLParser::grammar_arithmetic_factor_tail(DataType left_operand_type)
{
    auto token = lexer.peekNext();
    if ((token.term.type != TT_ID
            || !(token.term.payload == "mod" || token.term.payload == "end"
                   || token.term.payload == "and" || token.term.payload == "or"
                   || token.term.payload == "then" || token.term.payload == "do"))
        && token.term.type != TT_ARITHOP_MUL && token.term.type != TT_ARITHOP_DIV
        && token.term.type != TT_ARITHOP_PLUS && token.term.type != TT_ARITHOP_MINUS
        && token.term.type != TT_COMMA && token.term.type != TT_RIGHTPAREN
        && token.term.type != TT_RELOP_LT && token.term.type != TT_RELOP_EQ
        && token.term.type != TT_RELOP_GT && token.term.type != TT_SEMICOLON
        && token.term.type != TT_RIGHTBRACKET)
        throw expected_tokens(token, { Term::from_character.find('*')->second,
                                         Term::from_character.find('/')->second,
                                         { TT_ID, "mod" },
                                         Term::from_character.find('+')->second,
                                         Term::from_character.find('-')->second,
                                         Term::from_character.find(',')->second,
                                         Term::from_character.find(')')->second,
                                         Term::from_character.find('<')->second,
                                         Term::from_character.find('=')->second,
                                         Term::from_character.find('>')->second,
                                         Term::from_character.find(';')->second,
                                         { TT_ID, "end" }, { TT_ID, "and" }, { TT_ID, "or" },
                                         Term::from_character.find(']')->second,
                                         { TT_ID, "then" }, { TT_ID, "do" } });
    std::ostringstream out_buffer = stop_buffering();
    if (token.term.type == TT_ARITHOP_MUL || token.term.type == TT_ARITHOP_DIV
        || (token.term.type == TT_ID && token.term.payload == "mod")) {
        switch (token.term.type) {
        case TT_ARITHOP_MUL:
            get_current_out() << "<arithmetic_binary_operator_multiplication>" << std::endl;
            break;
        case TT_ARITHOP_DIV:
            get_current_out() << "<arithmetic_binary_operator_division>" << std::endl;
            break;
        case TT_ID:
            get_current_out() << "<arithmetic_binary_operator_modulo>" << std::endl;
            break;
        default:
            assert(false); // Should never reach here.
        }
        get_current_out() << "<left_operand>" << std::endl
                          << out_buffer.str()
                          << "</left_operand>" << std::endl
                          << "<right_operand>" << std::endl;
        lexer.getNext();
        DataType right_operand_type = grammar_arithmetic_factor();
        get_current_out() << "</right_operand>" << std::endl;
        switch (token.term.type) {
        case TT_ARITHOP_MUL:
            get_current_out() << "</arithmetic_binary_operator_multiplication>" << std::endl;
            break;
        case TT_ARITHOP_DIV:
            get_current_out() << "</arithmetic_binary_operator_division>" << std::endl;
            break;
        case TT_ID:
            get_current_out() << "</arithmetic_binary_operator_modulo>" << std::endl;
            break;
        default:
            assert(false); // Should never reach here.
        }
        return arithmetic_binary_operator_coerce(left_operand_type, right_operand_type);
    } else {
        // Avoid unnecessarily nested output if there is no nesting.
        get_current_out() << out_buffer.str();
        return left_operand_type;
    }
}

DataType XMLParser::grammar_arithmetic_operand()
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS)
        throw expected_tokens(token, { { TT_ID, "arbitrary" },
                                         { TT_INTEGER, "arbitrary" }, { TT_REAL, "arbitrary" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second });
    lexer.getNext();
    if (token.term.type == TT_LEFTPAREN) {
        DataType type = grammar_arithmetic_expression();
        token = lexer.getNext();
        if (token.term.type != TT_RIGHTPAREN)
            throw expected_tokens(token, { Term::from_character.find(')')->second });
        return type;
    } else if (token.term.type == TT_ARITHOP_MINUS) {
        get_current_out() << "<arithmetic_unary_operator_minus>" << std::endl;
        DataType type = grammar_arithmetic_operand();
        get_current_out() << "</arithmetic_unary_operator_minus>" << std::endl;
        return type;
    } else {
        SymbolData data;
        switch (token.term.type) {
        case TT_ID:
            get_current_out() << "<identifier>" << std::endl
                              << token.term.payload << std::endl
                              << "</identifier>" << std::endl;
            data = symbols.find(token.term.payload).second;
            if (data.symbol_type != ST_VARIABLE && data.symbol_type != ST_PARAMETER)
                throw undefined_variable(token);
            assert(data.data_type.size() == 1);
            return data.data_type.back();
        case TT_INTEGER:
            get_current_out() << "<integer_constant>" << std::endl
                              << token.term.payload << std::endl
                              << "</integer_constant>" << std::endl;
            return DT_INTEGER;
        case TT_REAL:
            get_current_out() << "<real_constant>" << std::endl
                              << token.term.payload << std::endl
                              << "</real_constant>" << std::endl;
            return DT_DOUBLE;
        default:
            assert(false); // Should never reach here.
        }
    }
    assert(false); // Should never reach here.
    return DT_ARBITRARY;
}

void XMLParser::grammar_bool_expression()
{
    start_buffering();
    grammar_bool_factor();
    grammar_bool_expression_tail();
}

void XMLParser::grammar_bool_expression_tail()
{
    auto token = lexer.peekNext();
    if ((token.term.type != TT_ID
            || !(token.term.payload == "or" || token.term.payload == "then"
                   || token.term.payload == "do"))
        && token.term.type != TT_RIGHTBRACKET)
        throw expected_tokens(token, { { TT_ID, "or" }, { TT_ID, "then" },
                                         { TT_ID, "do" },
                                         Term::from_character.find(']')->second });
    std::ostringstream out_buffer = stop_buffering();
    if (token.term.type == TT_ID && token.term.payload == "or") {
        get_current_out() << "<bool_binary_operator_or>" << std::endl
                          << "<left_operand>" << std::endl
                          << out_buffer.str()
                          << "</left_operand>" << std::endl
                          << "<right_operand>" << std::endl;
        lexer.getNext();
        grammar_bool_expression();
        get_current_out() << "</right_operand>" << std::endl
                          << "</bool_binary_operator_or>" << std::endl;
    } else {
        // Avoid unnecessarily nested output if there is no nesting.
        get_current_out() << out_buffer.str();
    }
}

void XMLParser::grammar_bool_factor()
{
    start_buffering();
    grammar_bool_operand();
    grammar_bool_factor_tail();
}

void XMLParser::grammar_bool_factor_tail()
{
    auto token = lexer.peekNext();
    if ((token.term.type != TT_ID
            || !(token.term.payload == "and" || token.term.payload == "or"
                   || token.term.payload == "then" || token.term.payload == "do"))
        && token.term.type != TT_RIGHTBRACKET)
        throw expected_tokens(token, { { TT_ID, "and" }, { TT_ID, "or" },
                                         { TT_ID, "then" }, { TT_ID, "do" },
                                         Term::from_character.find(']')->second });
    std::ostringstream out_buffer = stop_buffering();
    if (token.term.type == TT_ID && token.term.payload == "and") {
        get_current_out() << "<bool_binary_operator_and>" << std::endl
                          << "<left_operand>" << std::endl
                          << out_buffer.str()
                          << "</left_operand>" << std::endl
                          << "<right_operand>" << std::endl;
        lexer.getNext();
        grammar_bool_factor();
        get_current_out() << "</right_operand>" << std::endl
                          << "</bool_binary_operator_and>" << std::endl;
    } else {
        // Avoid unnecessarily nested output if there is no nesting.
        get_current_out() << out_buffer.str();
    }
}

void XMLParser::grammar_bool_operand()
{
    auto token = lexer.peekNext();
    if (token.term.type != TT_ID && token.term.type != TT_INTEGER
        && token.term.type != TT_REAL && token.term.type != TT_LEFTPAREN
        && token.term.type != TT_ARITHOP_MINUS && token.term.type != TT_LEFTBRACKET)
        throw expected_tokens(token, { { TT_ID, "arbitrary" },
                                         { TT_INTEGER, "arbitrary" }, { TT_REAL, "arbitrary" },
                                         Term::from_character.find('(')->second,
                                         Term::from_character.find('-')->second,
                                         Term::from_character.find('[')->second });
    if (token.term.type == TT_LEFTBRACKET) {
        lexer.getNext();
        grammar_bool_expression();
        token = lexer.getNext();
        if (token.term.type != TT_RIGHTBRACKET)
            throw expected_tokens(token, { Term::from_character.find(']')->second });
    } else if (token.term.type == TT_ID && token.term.payload == "not") {
        lexer.getNext();
        get_current_out() << "<bool_unary_operator_negation>" << std::endl;
        grammar_bool_operand();
        get_current_out() << "</bool_unary_operator_negation>" << std::endl;
    } else if (token.term.type == TT_ID
        && (token.term.payload == "true" || token.term.payload == "false")) {
        lexer.getNext();
        get_current_out() << "<bool_constant>" << std::endl
                          << token.term.payload << std::endl
                          << "</bool_constant>" << std::endl;
    } else {
        grammar_relation_expression();
    }
}

void XMLParser::grammar_relation_expression()
{
    start_buffering();
    grammar_arithmetic_expression();
    grammar_relation_expression_tail();
}

void XMLParser::grammar_relation_expression_tail()
{
    auto token = lexer.getNext();
    if (token.term.type != TT_RELOP_LT && token.term.type != TT_RELOP_EQ
        && token.term.type != TT_RELOP_GT)
        throw expected_tokens(token, { Term::from_character.find('<')->second,
                                         Term::from_character.find('=')->second,
                                         Term::from_character.find('>')->second });
    std::ostringstream out_buffer = stop_buffering();
    switch (token.term.type) {
    case TT_RELOP_LT:
        get_current_out() << "<relation_binary_operator_less_than>" << std::endl;
        break;
    case TT_RELOP_EQ:
        get_current_out() << "<relation_binary_operator_equals>" << std::endl;
        break;
    case TT_RELOP_GT:
        get_current_out() << "<relation_binary_operator_greater_than>" << std::endl;
        break;
    default:
        assert(false); // Should never reach here.
    }
    get_current_out() << "<left_operand>" << std::endl
                      << out_buffer.str()
                      << "</left_operand>" << std::endl
                      << "<right_operand>" << std::endl;
    grammar_arithmetic_expression();
    get_current_out() << "</right_operand>" << std::endl;
    switch (token.term.type) {
    case TT_RELOP_LT:
        get_current_out() << "</relation_binary_operator_less_than>" << std::endl;
        break;
    case TT_RELOP_EQ:
        get_current_out() << "</relation_binary_operator_equals>" << std::endl;
        break;
    case TT_RELOP_GT:
        get_current_out() << "</relation_binary_operator_greater_than>" << std::endl;
        break;
    default:
        assert(false); // Should never reach here.
    }
}
