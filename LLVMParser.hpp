#ifndef LLVM_PARSER_HPP
#define LLVM_PARSER_HPP

#include "Lexer.hpp"
#include "Parser.hpp"
#include <iostream>
#include <vector>

// This parser writes XML syntax tree produced from the Parser::in to Parser::out.
class LLVMParser : public Parser {
private:
    DataType arithmetic_binary_operator_coerce(
        DataType left_operand_type, DataType right_operator_type,
        size_t& left_operand_storage_offset,
        size_t& right_operand_storage_offset);
    DataType return_data_type_coerce(
        const Token& where, DataType expected_type, DataType actual_type,
        size_t& storage_offset);

    size_t llvm_store(size_t storage_offset, DataType type);
    size_t llvm_load(size_t storage_offset, DataType type);
    size_t llvm_coerce_i32_to_double(size_t storage_offset);
    void llvm_write_label(size_t label);
    void llvm_goto(size_t label);
    void llvm_conditional_goto(
        size_t loaded_storage_offset, size_t label_false, size_t label_true);

    void grammar_init();
    void grammar_block();
    void grammar_variable_declaration_list();
    void grammar_variable_declaration();
    DataType grammar_type();
    void grammar_callable_declaration_list();
    void grammar_callable_declaration();
    void grammar_parameter_list(
        std::vector<DataType>& types,
        std::vector<std::pair<std::string, size_t> >& parameter_names);
    void grammar_parameter_list_tail(std::vector<DataType>& types,
        std::vector<std::pair<std::string, size_t> >& parameter_names);
    void grammar_parameter(std::vector<DataType>& types,
        std::vector<std::pair<std::string, size_t> >& parameter_names);
    void grammar_statement_list(size_t label_next);
    void grammar_statement();
    void grammar_statement_if_tail(size_t label_next);
    DataType grammar_arithmetic_expression(size_t& storage_offset);
    DataType grammar_arithmetic_expression_tail(DataType left_operand_type,
        size_t left_operand_storage_offset, size_t& storage_offset);
    void grammar_arithmetic_expression_list(
        std::vector<DataType>& expected_types,
        std::vector<size_t>& storage_offsets);
    void grammar_arithmetic_expression_list_tail(
        std::vector<DataType>& expected_types,
        std::vector<size_t>& storage_offsets);
    DataType grammar_arithmetic_factor(size_t& storage_offset);
    DataType grammar_arithmetic_factor_tail(DataType left_operand_type,
        size_t left_operand_storage_offset, size_t& storage_offset);
    DataType grammar_arithmetic_operand(size_t& storage_offset);
    void grammar_bool_expression(
        size_t label_true, size_t label_false);
    void grammar_bool_expression_tail(
        size_t label_true,
        size_t label_false, size_t label_nested_false);
    void grammar_bool_factor(size_t label_true, size_t label_false);
    void grammar_bool_factor_tail(
        size_t label_true, size_t label_nested_true,
        size_t label_false);
    void grammar_bool_operand(
        size_t label_true, size_t label_false);
    size_t grammar_relation_expression();
    size_t grammar_relation_expression_tail(
        size_t left_operand_storage_offset, DataType left_operand_type);

    size_t current_label = 0;
    size_t get_next_label()
    {
        return current_label++;
    }

    size_t current_identifier = 0;
    size_t get_next_offset()
    {
        return current_identifier++;
    }

public:
    LLVMParser(Lexer& lexer, std::ostream& out, std::ostream& err)
        : Parser(lexer, out, err)
    {
    }
    void run() override;
};

#endif // LLVM_PARSER_HPP
