#ifndef XML_PARSER_HPP
#define XML_PARSER_HPP

#include "Lexer.hpp"
#include "Parser.hpp"
#include <vector>

// This parser writes XML syntax tree produced from the Parser::in to Parser::out.
class XMLParser : public Parser {
private:
    DataType arithmetic_binary_operator_coerce(
        DataType left_operand_type, DataType right_operator_type);
    DataType return_data_type_coerce(
        const Token& where, DataType expected_type, DataType actual_type);

    void grammar_init();
    void grammar_block();
    void grammar_variable_declaration_list(bool is_nested);
    void grammar_variable_declaration();
    DataType grammar_type();
    void grammar_callable_declaration_list(bool is_nested);
    void grammar_callable_declaration();
    void grammar_parameter_list(std::vector<DataType>& types);
    void grammar_parameter_list_tail(std::vector<DataType>& types);
    void grammar_parameter(std::vector<DataType>& types);
    void grammar_statement_list(bool is_nested);
    void grammar_statement();
    void grammar_statement_if_tail();
    void grammar_coerced_arithmetic_expression(const Token& where, DataType expected_type);
    DataType grammar_arithmetic_expression();
    DataType grammar_arithmetic_expression_tail(DataType left_operand_type);
    void grammar_arithmetic_expression_list(std::vector<DataType>& expected_types);
    void grammar_arithmetic_expression_list_tail(std::vector<DataType>& expected_types);
    DataType grammar_arithmetic_factor();
    DataType grammar_arithmetic_factor_tail(DataType left_operand_type);
    DataType grammar_arithmetic_operand();
    void grammar_bool_expression();
    void grammar_bool_expression_tail();
    void grammar_bool_factor();
    void grammar_bool_factor_tail();
    void grammar_bool_operand();
    void grammar_relation_expression();
    void grammar_relation_expression_tail();

public:
    XMLParser(Lexer& lexer, std::ostream& out, std::ostream& err)
        : Parser(lexer, out, err)
    {
    }
    void run() override;
};

#endif // XML_PARSER_HPP
