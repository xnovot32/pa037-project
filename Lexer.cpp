#include "Lexer.hpp"
#include <regex>
#include <sstream>

// Initialize the character to term mapping.
const std::map<int, Term> Term::from_character = [] {
    std::map<int, Term> map;
    map.insert(std::pair<int, Term>('(', { TT_LEFTPAREN, "(" }));
    map.insert(std::pair<int, Term>(')', { TT_RIGHTPAREN, ")" }));
    map.insert(std::pair<int, Term>('[', { TT_LEFTBRACKET, "[" }));
    map.insert(std::pair<int, Term>(']', { TT_RIGHTBRACKET, "]" }));
    map.insert(std::pair<int, Term>('+', { TT_ARITHOP_PLUS, "+" }));
    map.insert(std::pair<int, Term>('-', { TT_ARITHOP_MINUS, "-" }));
    map.insert(std::pair<int, Term>('*', { TT_ARITHOP_MUL, "*" }));
    map.insert(std::pair<int, Term>('/', { TT_ARITHOP_DIV, "/" }));
    map.insert(std::pair<int, Term>('<', { TT_RELOP_LT, "<" }));
    map.insert(std::pair<int, Term>('=', { TT_RELOP_EQ, "=" }));
    map.insert(std::pair<int, Term>('>', { TT_RELOP_GT, ">" }));
    map.insert(std::pair<int, Term>(':', { TT_COLON, ":" }));
    map.insert(std::pair<int, Term>(',', { TT_COMMA, "," }));
    map.insert(std::pair<int, Term>(';', { TT_SEMICOLON, ";" }));
    map.insert(std::pair<int, Term>(std::char_traits<char>::eof(), { TT_EOF, "End of file" }));
    return map;
}();

// Initialize the term type names.
const std::map<TermType, std::string> Term::type_names = [] {
    std::map<TermType, std::string> map;
    map.insert(std::pair<TermType, std::string>(TT_LEFTPAREN, "Left parenthesis"));
    map.insert(std::pair<TermType, std::string>(TT_RIGHTPAREN, "Right parenthesis"));
    map.insert(std::pair<TermType, std::string>(TT_LEFTBRACKET, "Left bracket"));
    map.insert(std::pair<TermType, std::string>(TT_RIGHTBRACKET, "Right bracket"));
    map.insert(std::pair<TermType, std::string>(TT_ARITHOP_PLUS, "Arithmetic addition operator"));
    map.insert(std::pair<TermType, std::string>(TT_ARITHOP_MINUS, "Arithmetic subtraction operator"));
    map.insert(std::pair<TermType, std::string>(TT_ARITHOP_MUL, "Arithmetic multiplication operator"));
    map.insert(std::pair<TermType, std::string>(TT_ARITHOP_DIV, "Arithmetic division operator"));
    map.insert(std::pair<TermType, std::string>(TT_RELOP_LT, "Relational less-than operator"));
    map.insert(std::pair<TermType, std::string>(TT_RELOP_EQ, "Relational equality operator"));
    map.insert(std::pair<TermType, std::string>(TT_RELOP_GT, "Relational greater-than operator"));
    map.insert(std::pair<TermType, std::string>(TT_COLON, "Colon"));
    map.insert(std::pair<TermType, std::string>(TT_COMMA, "Comma"));
    map.insert(std::pair<TermType, std::string>(TT_SEMICOLON, "Semicolon"));
    map.insert(std::pair<TermType, std::string>(TT_EOF, "End of file"));
    map.insert(std::pair<TermType, std::string>(TT_ID, "Identifier"));
    map.insert(std::pair<TermType, std::string>(TT_INTEGER, "Integer"));
    map.insert(std::pair<TermType, std::string>(TT_REAL, "Real number"));
    map.insert(std::pair<TermType, std::string>(TT_UNKNOWN, "Unknown term"));
    return map;
}();

Token Lexer::peekNext()
{
    if (!peek_buffer_full) {
        peek_buffer = Lexer::getNext();
        peek_buffer_full = true;
    }
    return peek_buffer;
}

Token Lexer::getNext()
{
    // If the user has already peeked at the next token, return that token.
    if (peek_buffer_full) {
        peek_buffer_full = false;
        return peek_buffer;
    }

    // Strip whitespace and newlines until we've hit a non-whitespace character.
    auto eof = std::char_traits<char>::eof();
    for (int peek = current_line.peek();
         peek == eof || peek == '\t' || peek == ' ';
         peek = current_line.peek()) {
        if (peek == eof) {
            std::string line_str;
            std::getline(in, line_str);
            current_line = std::istringstream(line_str);
            lineno += 1;
            // After we have reached the end of file, break the loop.
            if (in.peek() == eof)
                break;
        } else
            current_line.get();
    }

    // Read the first character.
    char c = current_line.peek();
    auto search = Term::from_character.find(c);
    if (search != Term::from_character.end()) {
        unsigned long charno = current_line.tellg();
        current_line.get();
        return { search->second, lineno, charno };
    }

    // Perform regex matching.
    static std::regex id("^[a-zA-Z][a-zA-Z0-9_]*");
    static std::regex integer("^[0-9]+");
    static std::regex real("^([0-9]*\\.[0-9]+)");
    std::string word;
    unsigned long charno = current_line.tellg();
    current_line >> word;
    std::smatch match;
    TermType type = TT_UNKNOWN;
    if (std::regex_search(word, match, id))
        type = TT_ID;
    else if (std::regex_search(word, match, real))
        type = TT_REAL;
    else if (std::regex_search(word, match, integer))
        type = TT_INTEGER;
    else {
        std::ostringstream ss;
        ss << "Unexpected character sequence encountered:\t" << word;
        warn(ss.str());
        return { { type, word }, lineno, charno };
    }
    // If the regex did not match the entire word rollback the stream.
    std::string matched_part = match[0].str();
    if (word.length() != matched_part.length())
        current_line.seekg(matched_part.length() - word.length(), std::ios_base::cur);
    if (type == TT_REAL && matched_part[0] == '.') {
        std::ostringstream ss;
        ss << "0" << matched_part;
        return Token{ { type, ss.str() }, lineno, charno };
    } else {
        return Token{ { type, matched_part }, lineno, charno };
    }
}

void Lexer::warn(std::string msg)
{
    err << "Lexer warning:\t" << msg << std::endl;
}
