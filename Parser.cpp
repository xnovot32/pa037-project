#include "Parser.hpp"
#include <exception>
#include <string>
#include <vector>

void Parser::warn(std::string msg)
{
    err << "Parser warning:\t" << msg << std::endl;
}

void Parser::error(std::string msg)
{
    err << "Parser error:\t" << msg << std::endl;
}

std::invalid_argument Parser::unexpected_token(const Token& actual, const std::string& msg)
{
    std::ostringstream ss;
    ss << "Unexpected token " << actual << "." << std::endl
       << msg;
    error(ss.str());
    return std::invalid_argument("Malformed input.");
}

std::invalid_argument Parser::expected_tokens(const Token& actual, const std::vector<Term>& expected)
{
    std::ostringstream ss;
    ss << "Expected one of the following:" << std::endl;
    for (Term t : expected)
        ss << "- " << t << std::endl;
    return unexpected_token(actual, ss.str());
}

std::invalid_argument Parser::undefined_variable(const Token& name)
{
    std::ostringstream ss;
    ss << "Variable " << name.term.payload << " has not been defined in this scope." << std::endl;
    return unexpected_token(name, ss.str());
}

std::invalid_argument Parser::multiply_defined_variable(const Token& name)
{
    std::ostringstream ss;
    ss << "Variable " << name.term.payload << " has already been defined in this scope." << std::endl;
    return unexpected_token(name, ss.str());
}

std::invalid_argument Parser::undefined_function(const Token& name)
{
    std::ostringstream ss;
    ss << "Function " << name.term.payload << " has not been defined in this scope." << std::endl;
    return unexpected_token(name, ss.str());
}

std::invalid_argument Parser::multiply_defined_function(const Token& name)
{
    std::ostringstream ss;
    ss << "Function " << name.term.payload << " has already been defined in this scope." << std::endl;
    return unexpected_token(name, ss.str());
}

std::invalid_argument Parser::type_error(const Token& where, std::string msg)
{
    std::ostringstream ss;
    ss << "Type error from " << where << ": " << msg << std::endl;
    error(ss.str());
    return std::invalid_argument("Malformed input.");
}

std::invalid_argument Parser::cannot_coerce(const Token& where, std::string from, std::string to)
{
    std::ostringstream ss;
    ss << "Cannot coerce " << from << " to " << to << "." << std::endl;
    return type_error(where, ss.str());
}

std::invalid_argument Parser::extra_parameter(const Token& where)
{
    std::ostringstream ss;
    ss << "Unexpected extra function call parameter." << std::endl;
    return type_error(where, ss.str());
}

std::invalid_argument Parser::missing_parameter(const Token& where)
{
    std::ostringstream ss;
    ss << "Expected extra function call parameter." << std::endl;
    return type_error(where, ss.str());
}
