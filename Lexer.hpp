#ifndef LEXER_HPP
#define LEXER_HPP

#include <iostream>
#include <map>
#include <regex>
#include <string>

enum TermType {
    TT_LEFTPAREN,
    TT_RIGHTPAREN,
    TT_LEFTBRACKET,
    TT_RIGHTBRACKET,
    TT_ARITHOP_PLUS,
    TT_ARITHOP_MINUS,
    TT_ARITHOP_MUL,
    TT_ARITHOP_DIV,
    TT_RELOP_LT,
    TT_RELOP_EQ,
    TT_RELOP_GT,
    TT_COLON,
    TT_COMMA,
    TT_SEMICOLON,
    TT_ID,
    TT_INTEGER,
    TT_REAL,
    TT_UNKNOWN,
    TT_EOF
};

struct Term {
    TermType type;
    static const std::map<TermType, std::string> type_names;
    std::string payload;
    static const std::map<int, Term> from_character;
};

inline std::ostream& operator<<(std::ostream& out, const Term& t)
{
    return out << "(" << Term::type_names.find(t.type)->second << ", \"" << t.payload << "\")";
}

inline bool operator==(const Term& l, const Term& r)
{
    return l.type == r.type && l.payload == r.payload;
}
inline bool operator!=(const Term& l, const Term& r)
{
    return !(l == r);
}

struct Token {
    Term term;
    unsigned long lineno;
    unsigned long charno;
};

inline std::ostream& operator<<(std::ostream& out, const Token& t)
{
    return out << t.term << " at line " << t.lineno
               << ", character number " << (t.charno + 1);
}

inline bool operator==(const Token& l, const Token& r)
{
    return l.term == r.term && l.lineno == r.lineno && l.charno == r.charno;
}
inline bool operator!=(const Token& l, const Token& r)
{
    return !(l == r);
}

class Lexer {
private:
    std::istream& in;
    std::ostream& out;
    std::ostream& err;
    std::istringstream current_line{ "" };
    unsigned long lineno = 0;
    bool peek_buffer_full = false;
    Token peek_buffer;

    void warn(std::string msg);

public:
    Lexer(std::istream& in, std::ostream& out, std::ostream& err)
        : in(in)
        , out(out)
        , err(err)
    {
    }
    Token peekNext();
    Token getNext();
};

#endif // LEXER_HPP
