#include "../LLVMParser.hpp"
#include "../Lexer.hpp"
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <exception>
#include <fstream>
#include <functional>
#include <gtest/gtest.h>
#include <locale>
#include <sstream>
#include <stdint.h>

class NullBuffer : public std::streambuf {
public:
    int overflow(int c) { return c; }
};

TEST(Phase03_LLVMParser, missingSemicolon)
{
    std::ifstream input_stream("tests/XMLParser/missing_semicolon.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"else\") at line 11, character number 1.", line);
}

TEST(Phase03_LLVMParser, wrongType)
{
    std::ifstream input_stream("tests/XMLParser/wrong_type.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"bool\") at line 2, character number 13.", line);
}

TEST(Phase03_LLVMParser, multiplyDefinedVariable)
{
    std::ifstream input_stream("tests/XMLParser/multiply_defined_variable.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"a\") at line 4, character number 10.", line);
}

TEST(Phase03_LLVMParser, multiplyDefinedFunction)
{
    std::ifstream input_stream("tests/XMLParser/multiply_defined_function.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"a\") at line 4, character number 10.", line);
}

TEST(Phase03_LLVMParser, scopes)
{
    std::ifstream input_stream("tests/XMLParser/scopes.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, std::cerr);
    parser.run();
}

TEST(Phase03_LLVMParser, undefinedVariable)
{
    std::ifstream input_stream("tests/XMLParser/undefined_variable.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"z\") at line 5, character number 14.", line);
}

TEST(Phase03_LLVMParser, undefinedFunction)
{
    std::ifstream input_stream("tests/XMLParser/undefined_function.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"mul\") at line 16, character number 15.", line);
}

TEST(Phase03_LLVMParser, cannotCoerceAssignment)
{
    std::ifstream input_stream("tests/XMLParser/cannot_coerce_assignment.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Relational equality operator, \"=\") at line 2, character number 3: Cannot coerce double to integer.", line);
}

TEST(Phase03_LLVMParser, cannotCoerceReturn)
{
    std::ifstream input_stream("tests/XMLParser/cannot_coerce_return.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Identifier, \"return\") at line 2, character number 3: Cannot coerce double to integer.", line);
}

TEST(Phase03_LLVMParser, cannotCoerceCall)
{
    std::ifstream input_stream("tests/XMLParser/cannot_coerce_call.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Real number, \"1.23456\") at line 7, character number 12: Cannot coerce double to integer.", line);
}

TEST(Phase03_LLVMParser, extraParameter)
{
    std::ifstream input_stream("tests/XMLParser/extra_parameter.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Integer, \"13\") at line 7, character number 16: Unexpected extra function call parameter.", line);
}

TEST(Phase03_LLVMParser, missingParameter)
{
    std::ifstream input_stream("tests/XMLParser/missing_parameter.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Right parenthesis, \")\") at line 7, character number 14: Expected extra function call parameter.", line);
}

TEST(Phase03_LLVMParser, parameterlessCall)
{
    std::ifstream input_stream("tests/XMLParser/parameterless_call.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    LLVMParser parser(lexer, null_stream, std::cerr);
    parser.run();
}

std::string& ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                           std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}
std::string& rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
                std::not1(std::ptr_fun<int, int>(std::isspace)))
                .base(),
        s.end());
    return s;
}
std::string& trim(std::string& s)
{
    return ltrim(rtrim(s));
}

std::string compile_and_run(const std::string& input)
{
    {
        std::istringstream input_stream(input);
        Lexer lexer(input_stream, std::cout, std::cerr);
        std::ofstream output_stream("tests/LLVMParser.ll");
        LLVMParser parser(lexer, output_stream, std::cerr);
        parser.run();
    }
    system("lli tests/LLVMParser.ll > tests/LLVMParser.out");
    std::ifstream output_file("tests/LLVMParser.out");
    std::string output(
        (std::istreambuf_iterator<char>(output_file)), std::istreambuf_iterator<char>());
    return trim(output);
}

int32_t string_to_integer(const std::string& input)
{
    int32_t output;
    if (input.empty() || input.find('.') != std::string::npos) {
        std::ostringstream ss;
        ss << "The input '" << input << "' is not an integer.";
        throw std::invalid_argument(ss.str());
    }
    std::istringstream(input) >> output;
    return output;
}

double string_to_double(const std::string& input)
{
    double output;
    if (input.empty() || input.find('.') == std::string::npos) {
        std::ostringstream ss;
        ss << "The input '" << input << "' is not a double.";
        throw std::invalid_argument(ss.str());
    }
    std::istringstream(input) >> output;
    return output;
}

TEST(Phase03_LLVMParser, globalReturns)
{
    EXPECT_EQ(12345, string_to_integer(compile_and_run("return 12345;")));
    EXPECT_EQ(1.2345, string_to_double(compile_and_run("return 1.2345;")));
    EXPECT_EQ(0.12345, string_to_double(compile_and_run("return .12345;")));
}

TEST(Phase03_LLVMParser, localReturns)
{
    EXPECT_EQ(12345, string_to_integer(compile_and_run("function f() : integer return 12345; end; return call f();")));
    EXPECT_EQ(1.2345, string_to_double(compile_and_run("function f() : double return 1.2345; end; return call f();")));
}

TEST(Phase03_LLVMParser, defaultLocalReturns)
{
    EXPECT_EQ(0, string_to_integer(compile_and_run("function f() : integer end; return call f();")));
    EXPECT_EQ(0.0, string_to_double(compile_and_run("function f() : double end; return call f();")));
}

TEST(Phase03_LLVMParser, globalVariables)
{
    EXPECT_EQ(12345, string_to_integer(compile_and_run("variable a : integer; a = 12345; return a;")));
    EXPECT_EQ(1.2345, string_to_double(compile_and_run("variable a : double; a = 1.2345; return a;")));
    EXPECT_EQ(12345, string_to_integer(compile_and_run("variable a : integer; variable temp : integer; function f() : integer a = 12345; end; temp = call f(); return a;")));
    EXPECT_EQ(1.2345, string_to_double(compile_and_run("variable a : double; variable temp : double; function f() : double a = 1.2345; end; temp = call f(); return a;")));
}

TEST(Phase03_LLVMParser, localVariables)
{
    EXPECT_EQ(12345, string_to_integer(compile_and_run("function f() : integer variable a : integer; a = 12345; return a; end; return call f();")));
    EXPECT_EQ(1.2345, string_to_double(compile_and_run("function f() : double variable a : double; a = 1.2345; return a; end; return call f();")));
}

TEST(Phase03_LLVMParser, integerArithmetic)
{
    EXPECT_EQ(6, string_to_integer(compile_and_run("return 1+2+3;")));
    EXPECT_EQ(2, string_to_integer(compile_and_run("return 1+-2+3;")));
    EXPECT_EQ(-4, string_to_integer(compile_and_run("return 1+-(2+3);")));
    EXPECT_EQ(11, string_to_integer(compile_and_run("return 1+2*3+4;")));

    EXPECT_EQ(1, string_to_integer(compile_and_run("return 6/4;")));
    EXPECT_EQ(-1, string_to_integer(compile_and_run("return -6/4;")));
    EXPECT_EQ(-1, string_to_integer(compile_and_run("return 6/-4;")));

    EXPECT_EQ(5, string_to_integer(compile_and_run("return 5 mod 6;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("return 6 mod 6;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("return 7 mod 6;")));
    EXPECT_EQ(-5, string_to_integer(compile_and_run("return -5 mod 6;")));
    EXPECT_EQ(5, string_to_integer(compile_and_run("return 5 mod -6;")));
    EXPECT_EQ(-5, string_to_integer(compile_and_run("return -5 mod -6;")));
}

TEST(Phase03_LLVMParser, doubleArithmetic)
{
    EXPECT_EQ(6.0, string_to_double(compile_and_run("return 1.0+1.5+3.5;")));
    EXPECT_EQ(2.0, string_to_double(compile_and_run("return 1.0+-2.5+3.5;")));
    EXPECT_EQ(-4.0, string_to_double(compile_and_run("return 1.0+-(1.5+3.5);")));
    EXPECT_EQ(11.0, string_to_double(compile_and_run("return 1.0+2.0*3.0+4.0;")));

    EXPECT_EQ(1.5, string_to_double(compile_and_run("return 6.0/4.0;")));
    EXPECT_EQ(-1.5, string_to_double(compile_and_run("return -6.0/4.0;")));
    EXPECT_EQ(-1.5, string_to_double(compile_and_run("return 6.0/-4.0;")));

    EXPECT_EQ(5.5, string_to_double(compile_and_run("return 5.5 mod 6.0;")));
    EXPECT_EQ(0.5, string_to_double(compile_and_run("return 6.5 mod 6.0;")));
    EXPECT_EQ(1.5, string_to_double(compile_and_run("return 7.5 mod 6.0;")));
    EXPECT_EQ(-5.5, string_to_double(compile_and_run("return -5.5 mod 6.0;")));
    EXPECT_EQ(5.5, string_to_double(compile_and_run("return 5.5 mod -6.0;")));
    EXPECT_EQ(-5.5, string_to_double(compile_and_run("return -5.5 mod -6.0;")));
}

TEST(Phase03_LLVMParser, chainedCalls)
{
    EXPECT_EQ(12345, string_to_integer(compile_and_run("function f() : integer return 12345; end; function g() : integer return call f(); end; return call g();")));
    EXPECT_EQ(1.2345, string_to_double(compile_and_run("function f() : double return 1.2345; end; function g() : double return call f(); end; return call g();")));
}

TEST(Phase03_LLVMParser, coercionsInt32ToDouble)
{
    for (std::string input : {
             "function f() : double return 12345; end; return call f();",
             "function f() : double return 12345; end; return call f();",
             "return 12345 + 0.0;",
             "return 12345 mod 12346.0;",
             "return 12345 * 1.0;",
             "return 12345 / 1.0;" }) {
        EXPECT_EQ(12345, string_to_double(compile_and_run(input)));
        EXPECT_THROW(string_to_integer(compile_and_run(input)), std::invalid_argument);
    }
}

TEST(Phase03_LLVMParser, if)
{
    EXPECT_EQ(1, string_to_integer(compile_and_run("if true then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if false then return 1; fi; return 0;")));
}

TEST(Phase03_LLVMParser, ifElse)
{
    EXPECT_EQ(1, string_to_integer(compile_and_run("if true then return 1; else return 0; fi;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if false then return 1; else return 0; fi;")));
}

TEST(Phase03_LLVMParser, while)
{
    EXPECT_EQ(1, string_to_integer(compile_and_run("while true do return 1; done; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("while false do return 1; done; return 0;")));

    EXPECT_EQ(4, string_to_integer(compile_and_run("variable i : integer; while i < 4 do i = i + 1; done; return i;")));
}

TEST(Phase03_LLVMParser, boolExpressions)
{
    EXPECT_EQ(1, string_to_integer(compile_and_run("if true and true then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if true and false then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if false and true then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if false and false then return 1; fi; return 0;")));

    EXPECT_EQ(1, string_to_integer(compile_and_run("if true or true then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if true or false then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if false or true then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if false or false then return 1; fi; return 0;")));

    EXPECT_EQ(0, string_to_integer(compile_and_run("if not [true and true] then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if not [true and false] then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if not [false and true] then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if not [false and false] then return 1; fi; return 0;")));

    EXPECT_EQ(0, string_to_integer(compile_and_run("if not [true or true] then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if not [true or false] then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if not [false or true] then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if not [false or false] then return 1; fi; return 0;")));
}

TEST(Phase03_LLVMParser, relationExpressions)
{
    EXPECT_EQ(1, string_to_integer(compile_and_run("if 1 < 2 then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if 1 = 2 then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if 1 > 2 then return 1; fi; return 0;")));

    EXPECT_EQ(0, string_to_integer(compile_and_run("if 2 < 2 then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if 2 = 2 then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if 2 > 2 then return 1; fi; return 0;")));

    EXPECT_EQ(0, string_to_integer(compile_and_run("if 2 < 1 then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if 2 = 1 then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if 2 > 1 then return 1; fi; return 0;")));

    EXPECT_EQ(1, string_to_integer(compile_and_run("if 1.0 < 2.0 then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if 1.0 = 2.0 then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if 1.0 > 2.0 then return 1; fi; return 0;")));

    EXPECT_EQ(0, string_to_integer(compile_and_run("if 2.0 < 2.0 then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if 2.0 = 2.0 then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if 2.0 > 2.0 then return 1; fi; return 0;")));

    EXPECT_EQ(0, string_to_integer(compile_and_run("if 2.0 < 1.0 then return 1; fi; return 0;")));
    EXPECT_EQ(0, string_to_integer(compile_and_run("if 2.0 = 1.0 then return 1; fi; return 0;")));
    EXPECT_EQ(1, string_to_integer(compile_and_run("if 2.0 > 1.0 then return 1; fi; return 0;")));
}

TEST(Phase02_XMLParser, sampleCountChange)
{
    std::ifstream input_stream("tests/LLVMParser/count_change.in");
    std::string input(
        (std::istreambuf_iterator<char>(input_stream)), std::istreambuf_iterator<char>());
    EXPECT_EQ(292, string_to_integer(compile_and_run(input)));
}

TEST(Phase02_XMLParser, sampleFactorial)
{
    std::ifstream input_stream("tests/LLVMParser/factorial.in");
    std::string input(
        (std::istreambuf_iterator<char>(input_stream)), std::istreambuf_iterator<char>());
    EXPECT_EQ(120, string_to_integer(compile_and_run(input)));
}

TEST(Phase02_XMLParser, sampleSmallestFactor)
{
    std::ifstream input_stream("tests/LLVMParser/smallest_factor.in");
    std::string input(
        (std::istreambuf_iterator<char>(input_stream)), std::istreambuf_iterator<char>());
    EXPECT_EQ(739, string_to_integer(compile_and_run(input)));
}

TEST(Phase02_XMLParser, quasiNewtonMethod)
{
    std::ifstream input_stream("tests/LLVMParser/quasi_newton_method.in");
    std::string input(
        (std::istreambuf_iterator<char>(input_stream)), std::istreambuf_iterator<char>());
    EXPECT_EQ(2.154435, string_to_double(compile_and_run(input)));
}
