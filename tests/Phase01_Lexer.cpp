#include "../Lexer.hpp"
#include <gtest/gtest.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

class NullBuffer : public std::streambuf {
public:
    int overflow(int c) { return c; }
};

TEST(Phase01_Lexer, characterTokens)
{
    for (std::pair<std::string, Term> expected : std::vector<std::pair<std::string, Term> >{
             { "(1+2)", Term::from_character.find('(')->second },
             { ") / (3+4)", Term::from_character.find(')')->second },
             { "[ 1+2 > 3 and true ]", Term::from_character.find('[')->second },
             { "] or not false", Term::from_character.find(']')->second },
             { "+1", Term::from_character.find('+')->second },
             { "-2", Term::from_character.find('-')->second },
             { "*3", Term::from_character.find('*')->second },
             { "/4", Term::from_character.find('/')->second },
             { "< 5", Term::from_character.find('<')->second },
             { "= 6", Term::from_character.find('=')->second },
             { "> 7", Term::from_character.find('>')->second },
             { ":= 8", Term::from_character.find(':')->second },
             { ", 10, 11", Term::from_character.find(',')->second },
             { "; 12", Term::from_character.find(';')->second },
         }) {
        std::istringstream input_stream(expected.first);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term term = lexer.getNext().term;
        EXPECT_EQ(expected.second, term);
    }
}

TEST(Phase01_Lexer, identifiers)
{
    TermType expected_type = TT_ID;
    for (std::string str : { "s0me_Identifier", "x" }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, str };
        EXPECT_EQ(expected, actual);
    }
    for (std::string str : { "0notAnIdentifier", "not-an-identifier" }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, str };
        EXPECT_NE(expected, actual);
    }
}

TEST(Phase01_Lexer, integers)
{
    TermType expected_type = TT_INTEGER;
    for (std::string str : { "123456" }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, str };
        EXPECT_EQ(expected, actual);
    }
    for (std::string str : { "1.23456", "-123456" }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, str };
        EXPECT_NE(expected, actual);
    }
}

TEST(Phase01_Lexer, reals)
{
    TermType expected_type = TT_REAL;
    for (std::string str : { "1.23456" }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, str };
        EXPECT_EQ(expected, actual);
    }
    for (std::string str : { "0.0", ".0" }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, "0.0" };
        EXPECT_EQ(expected, actual);
    }
    for (std::string str : { "-1.23456", "0.", "." }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, str };
        EXPECT_NE(expected, actual);
    }
}

TEST(Phase01_Lexer, unknowns)
{
    TermType expected_type = TT_UNKNOWN;
    for (std::string str : { "{" }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, str };
        EXPECT_EQ(expected, actual);
    }
}

TEST(Phase01_Lexer, eof)
{
    std::istringstream input_stream("");
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    Lexer lexer(input_stream, null_stream, null_stream);
    Term actual = lexer.getNext().term;
    auto eof = std::char_traits<char>::eof();
    EXPECT_EQ(Term::from_character.find(eof)->second, actual);
}

TEST(Phase01_Lexer, spacing)
{
    TermType expected_type = TT_ID;
    for (std::string str : { "s0me_Identifier", " \t \n s0me_Identifier", "s0me_Identifier;" }) {
        std::istringstream input_stream(str);
        NullBuffer null_buffer;
        std::ostream null_stream(&null_buffer);
        Lexer lexer(input_stream, null_stream, null_stream);
        Term actual = lexer.getNext().term;
        Term expected = { expected_type, "s0me_Identifier" };
        EXPECT_EQ(expected, actual);
    }
}
