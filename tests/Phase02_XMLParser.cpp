#include "../Lexer.hpp"
#include "../XMLParser.hpp"
#include <exception>
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
#include <sstream>

class NullBuffer : public std::streambuf {
public:
    int overflow(int c) { return c; }
};

TEST(Phase02_XMLParser, mixedBag)
{
    std::ifstream input_stream("tests/XMLParser/mixed_bag.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream output_stream;
    XMLParser parser(lexer, output_stream, std::cerr);
    parser.run();

    std::ifstream expected_output_file("tests/XMLParser/mixed_bag.out");
    std::string expected_output(
        (std::istreambuf_iterator<char>(expected_output_file)), std::istreambuf_iterator<char>());
    EXPECT_EQ(expected_output, output_stream.str());
}

TEST(Phase02_XMLParser, missingSemicolon)
{
    std::ifstream input_stream("tests/XMLParser/missing_semicolon.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"else\") at line 11, character number 1.", line);
}

TEST(Phase02_XMLParser, wrongType)
{
    std::ifstream input_stream("tests/XMLParser/wrong_type.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"bool\") at line 2, character number 13.", line);
}

TEST(Phase02_XMLParser, multiplyDefinedVariable)
{
    std::ifstream input_stream("tests/XMLParser/multiply_defined_variable.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"a\") at line 4, character number 10.", line);
}

TEST(Phase02_XMLParser, multiplyDefinedFunction)
{
    std::ifstream input_stream("tests/XMLParser/multiply_defined_function.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"a\") at line 4, character number 10.", line);
}

TEST(Phase02_XMLParser, scopes)
{
    std::ifstream input_stream("tests/XMLParser/scopes.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, std::cerr);
    parser.run();
}

TEST(Phase02_XMLParser, undefinedVariable)
{
    std::ifstream input_stream("tests/XMLParser/undefined_variable.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"z\") at line 5, character number 14.", line);
}

TEST(Phase02_XMLParser, undefinedFunction)
{
    std::ifstream input_stream("tests/XMLParser/undefined_function.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tUnexpected token (Identifier, \"mul\") at line 16, character number 15.", line);
}

TEST(Phase02_XMLParser, cannotCoerceAssignment)
{
    std::ifstream input_stream("tests/XMLParser/cannot_coerce_assignment.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Relational equality operator, \"=\") at line 2, character number 3: Cannot coerce double to integer.", line);
}

TEST(Phase02_XMLParser, cannotCoerceReturn)
{
    std::ifstream input_stream("tests/XMLParser/cannot_coerce_return.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Identifier, \"return\") at line 2, character number 3: Cannot coerce double to integer.", line);
}

TEST(Phase02_XMLParser, cannotCoerceCall)
{
    std::ifstream input_stream("tests/XMLParser/cannot_coerce_call.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Real number, \"1.23456\") at line 7, character number 12: Cannot coerce double to integer.", line);
}

TEST(Phase02_XMLParser, extraParameter)
{
    std::ifstream input_stream("tests/XMLParser/extra_parameter.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Integer, \"13\") at line 7, character number 16: Unexpected extra function call parameter.", line);
}

TEST(Phase02_XMLParser, missingParameter)
{
    std::ifstream input_stream("tests/XMLParser/missing_parameter.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    std::ostringstream error_stream_out;
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, error_stream_out);
    EXPECT_THROW(parser.run(), std::invalid_argument);

    std::istringstream error_stream_in(error_stream_out.str());
    std::string line;
    std::getline(error_stream_in, line);
    EXPECT_EQ("Parser error:\tType error from (Right parenthesis, \")\") at line 7, character number 14: Expected extra function call parameter.", line);
}

TEST(Phase02_XMLParser, parameterlessCall)
{
    std::ifstream input_stream("tests/XMLParser/parameterless_call.in");
    Lexer lexer(input_stream, std::cout, std::cerr);
    NullBuffer null_buffer;
    std::ostream null_stream(&null_buffer);
    XMLParser parser(lexer, null_stream, std::cerr);
    parser.run();
}
