#include "LLVMParser.hpp"
#include "Lexer.hpp"
#include "XMLParser.hpp"
#include <iostream>
#include <sstream>

int main(int argc, char** argv)
{
    Lexer lexer(std::cin, std::cout, std::cerr);
    std::string use_xml = "--xml";
    if (argc == 2 && argv[1] == use_xml) {
        XMLParser parser(lexer, std::cout, std::cerr);
        parser.run();
    } else {
        LLVMParser parser(lexer, std::cout, std::cerr);
        parser.run();
    }
    return 0;
}
